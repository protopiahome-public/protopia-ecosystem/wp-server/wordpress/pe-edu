<?php
class Bio_Role_Taxonomy extends SMC_Taxonomy
{
	static $roles;
	static function get_type()
	{
		return BIO_ROLE_TAXONOMY_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 19);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 19);
		add_filter("manage_edit-".BIO_ROLE_TAXONOMY_TYPE."_columns", 	array( __CLASS__,'ctg_columns')); 
		add_filter("manage_".BIO_ROLE_TAXONOMY_TYPE."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		add_action( BIO_ROLE_TAXONOMY_TYPE.'_edit_form_fields', 		array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_'.BIO_ROLE_TAXONOMY_TYPE, 					array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_'.BIO_ROLE_TAXONOMY_TYPE, 					array( __CLASS__, 'save_ctg'), 10);	
		if(!is_admin())
		{
			add_action( 'pre_get_posts', 	[__CLASS__,'pregetposts'] );
		}
		parent::init();
	}
	static function getRoles()
	{
		static::$roles = [];
		$terms = get_terms(static::get_type(), [
			'taxonomy' => static::get_type(),
			'hide_empty' => false,
			"fields"	=> "all"
		]);
		foreach($terms as $term)
		{
			$role = get_term_meta($term->term_id, "role", true);
			if( count(Bio_User::is_user_roles([$role, "administrator"  ], wp_get_current_user())) == 0)
			{
				static::$roles[ ] = $term->term_id;
			}
		}
		return static::$roles;
	}
	static function pregetposts($query)
	{
		static::getRoles();
		if(count( static::$roles))
		{
			$q = $query->get("tax_query");
			if($q)
			{
				$q[0][] = [
					"taxonomy" => static::get_type(), 
					"fields" => "term_id", 
					"terms"=> static::$roles, 
					'operator' => 'NOT IN' 
				];
				$query->set("tax_query", $q);
			}
		}
		//$query->set( static::get_type(), implode(",", static::$roles) );
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( BIO_ARTICLE_TYPE ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Role Taxonomy", BIO),
					'singular_name'     => __("Role Taxonomy", BIO),
					'search_items'      => __('search Role Taxonomy', BIO),
					'all_items'         => __('all Role Taxonomies', BIO),
					'view_item '        => __('view Role Taxonomy', BIO),
					'parent_item'       => __('parent Role Taxonomy', BIO),
					'parent_item_colon' => __('parent Role Taxonomy:', BIO),
					'edit_item'         => __('edit Role Taxonomy', BIO),
					'update_item'       => __('update Role Taxonomy', BIO),
					'add_new_item'      => __('add Role Taxonomy', BIO),
					'new_item_name'     => __('new Role Taxonomy Name', BIO),
					'menu_name'         => __('Role Taxonomy', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Role Taxonomies", BIO), 
			__("Role Taxonomies", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-".BIO_ROLE_TAXONOMY_TYPE."", __("Role Taxonomies", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
	
	
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 				=> ' ',
			//'id' 				=> 'id',
			'name' 				=> __('Name'),
			'icon' 				=> __('Icon', BIO),
			'role' 				=> __('role', BIO),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'role': 
				$role = get_term_meta( $term_id, 'role', true ); 
				$out 		.=  $role;
				break;	 
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	 
			case "icon":
				$icon = get_term_meta( $term_id, 'icon', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />";
				break;	
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id = $term->term_id;
			$color = get_term_meta($term_id, "color", true);
			$icon  = get_term_meta($term_id, "icon", true);
			$role  = get_term_meta($term_id, "role", true);
			$icon  = is_wp_error($icon) ? "" :  $icon;
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="role">
						<?php echo __("Role", BIO);  ?>
				</label> 
			</th>
			<td>
				<select name="role" id="role">
					<option value="-1">--</option>
					<?php wp_dropdown_roles( $role ) ?>
				</select>
			</td>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="icon">
					<?php echo __("Icon", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $icon, "group_icon", 0 );
				?>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		//var_dump($_POST);
		//wp_die();
		update_term_meta($term_id, "role", $_POST['role']);
		update_term_meta($term_id, "icon",  $_POST['group_icon0']);
	}
	
    static function delete( $post_id )
    {
        $post_id = (int)$post_id;
        wp_delete_term( $post_id, static::get_type() );
		update_term_meta($post_id, "role", $data["role"]);
		update_term_meta($post_id, "icon", $data["icon"]);
        return $post_id;
    }

    static function update( $data, $post_id )
    {
        $post_id = (int)$post_id;
        $data["name"] = $data["post_title"];// ? $data["post_title"] : $data["name"];
        wp_update_term( $post_id, static::get_type(), array(
            'name' 			=> $data["name"],
            'description' 	=> $data["description"],
        ));
        update_term_meta($post_id, "icon", $data["icon"]);
        update_term_meta($post_id, "role", $data["role"]);
        return $post_id;
    }
    static function insert( $data )
    {
        $data['name'] = $data['post_title'];
        $post_id = wp_insert_term(
            $data["name"], static::get_type(),
            array(
            'description' => $data["description"]
        ) );
        update_term_meta($post_id, "icon", $data["icon"]);
        update_term_meta($post_id, "role", $data["role"]);
        return $post_id;
    }


    static function get_classes($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, BIO_ROLE_TAXONOMY_TYPE);
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;

        $c['id']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['icon']			= get_term_meta($course->term_id, "icon", true);
        $c['thumbnail_id']	= $c['icon'];
        $c['thumbnail']		= wp_get_attachment_url($c['icon']);
        $c['post_content']	= $course->description;

        return $c;
    }

    static function get_class($class)
    {
        if(is_numeric($class))
        {
            $class = get_term($class, BIO_ROLE_TAXONOMY_TYPE);
        }
        $c = [];
        $c['ID']			= $class->term_id;
        $c['id']			= $class->term_id;
        $c['post_title']	= $class->name;
        $c['icon']			= get_term_meta($class->term_id, "icon", true);
        $c['thumbnail_id']	= $c['icon'];
        $c['thumbnail']		= wp_get_attachment_url($c['icon']);
        $c['role']			= get_term_meta($class->term_id, "role", true);
        return $c;
    }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
        $courses	= [];

        switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_OLIMPIAD_TYPE_EDIT, "Update Role Taxonomy");
					static::update($pars, $code);
					$cat = static::get_class( $code );
					$msg = sprintf( __("Role taxonomy «%s» updated succesfully", BIO), $cat['post_title'] );
					$courses[]	= $cat;
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_OLIMPIAD_TYPE_DELETE, "Delete Role Taxonomy");
                    static::delete($code);
                    $msg = 'success';
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "create":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps( BIO_ROLE_TAXONOMY_CREATE, "Insert Role Taxonomy" );
                    static::update($pars, $code);
                    $cat = static::get_class( $code );
					$msg = sprintf( __("Role taxonomy «%s» updated succesfully", BIO), $cat['post_title'] );
					$courses[]	= $cat;
                }
				else
				{
					Bio_User::access_caps( BIO_OLIMPIAD_TYPE_EDIT, "Update Role Taxonomy" );
                    $class = static::insert($pars);
                    $courses[]			= static::get_class($class);
                    $msg = 'success';
                }
                break;
            case "read":
            default:
//                        $code = (int)$code;
                if(is_numeric($code))
				{
                    /**/
                    $articles = [];
                    $all 	= Bio_Article::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "title", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        isset($pars['author'])			? $pars['author']		: -1,		// "",
                        [ "bio_class" => $code ]

                    );
                    foreach($all as $p)
                    {
                        $a 					= Bio_Article::get_article( $p, false );
                        $courses[]			= $a;
                    }
                    $c							= static::get_class( $code );
					$c['count']					= Bio_Course::get_article_count( $code );
                    $courses[]					= $c;
                }
				else
				{
                    $terms = get_terms( array(
                        'taxonomy'      => BIO_ROLE_TAXONOMY_TYPE,
                        'orderby'       => 'id',
                        'order'         => 'ASC',
                        'hide_empty'    => false,
                        'object_ids'    => null,
                        'include'       => array(),
                        'exclude'       => array(),
                        'exclude_tree'  => array(),
                        'number'        => '',
                        'fields'        => 'all',
                        'count'         => false,
                        'slug'          => '',
                        'parent'         => '',
                        'hierarchical'  => true,
                        'child_of'      => 0,
                        'offset'        => '',
                        'name'          => '',
                        'childless'     => false,
                        'update_term_meta_cache' => true,
                        'meta_query'    => '',
                    ) );
                    foreach($terms as $c)
                    {
                        $courses[]	= static::get_class( $c );
                    }
                    break;
                }
                break;
        }
        return Array (
            "bio_role_taxonomy" => $courses,
            "articles" => $articles,
            "id" => $code,
            "msg"=> $msg
        );
    }
}
	