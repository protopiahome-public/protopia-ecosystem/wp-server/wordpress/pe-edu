<?php
class Bio_Category extends SMC_Taxonomy
{
	
	static function get_type()
	{
		return "category";
	}
	static function init()
	{
		add_filter("manage_edit-category_columns", 		array( __CLASS__,'ctg_columns')); 
		add_filter("manage_category_custom_column",		array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		add_action( 'category_edit_form_fields', 		array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_category', 					array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_category', 					array( __CLASS__, 'save_ctg'), 10);	
	}
	
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Biology Themes", BIO), 
			__("Biology Themes", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-".$BIO_BIOLOGY_THEME_TYPE."", __("Biology Themes", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', $tax );	
    }
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			//'id' 			=> 'id',
			'name' 			=> __('Name'),
			'icon' 			=> __('Icon', BIO),
			'order' 		=> __('Order', BIO),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break; 
			case "icon":
				$icon = get_term_meta( $term_id, 'icon', true ); 
				$color = get_term_meta( $term_id, 'color', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />
				<div class='w-50 pb-2' style='background-color:$color;'><div>";
				break;	 
			case 'order': 
				$order = get_term_meta( $term_id, 'order', true ); 
				$out 		.= $order;
				break;	 
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id = $term->term_id;
			$icon  = get_term_meta($term_id, "icon", true);
			$color  = get_term_meta($term_id, "color", true);
			$order  = get_term_meta($term_id, "order", true);
			$icon  = is_wp_error($icon) ? "" :  $icon;
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="icon">
					<?php echo __("Icon", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $icon, "group_icon", 0 );
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="color">
					<?php echo __("Color", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="color" name="color" value="<?php echo $color; ?>" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("Order", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" value="<?php echo $order; ?>" name="order" id="order"/>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "icon",  $_POST['group_icon0']);
		update_term_meta($term_id, "color",  $_POST['color']);
		update_term_meta($term_id, "order",  $_POST['order']);
	}

    static function delete( $post_id )
    {
        $post_id = (int)$post_id;
        wp_delete_term( $post_id, static::get_type() );
//        update_term_meta($post_id, "icon", $data["icon"]);
        return $post_id;
    }

	static function update( $data, $post_id )
	{
		$post_id = (int)$post_id;
        $data['name'] = $data['post_title'];

        if( $data['icon_id'] < 1 )
        {
            $media = Bio_Assistants::insert_media([ "data" => $data['icon'], "media_name"=> $data['media_name']]);wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
            $data['icon_id']	= $media['id'];
            $data['icon']		= $media['url'];
        }

        wp_update_term( $post_id, static::get_type(), array(
            'name' 			=> $data["name"],
            'description' 	=> $data["description"],
        ));

        update_term_meta($post_id, "icon", $data["icon_id"]);
		update_term_meta($post_id, "color", $data["color"]);
        $data["order"] > -1 ? update_term_meta($post_id, "order", $data["order"]) : delete_term_meta($post_id, "order", true);

		return $post_id;
	}
	static function insert( $data )
	{
        $data['name'] = $data['post_title'];
        if( $data['icon_id'] < 1 )
        {
            $media = Bio_Assistants::insert_media([ "data" => $data['icon'], "media_name"=> $data['media_name']]);wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
            $data['icon_id']	= $media['id'];
            $data['icon']		= $media['url'];
        }

        $post_id = wp_insert_term( $data["name"], static::get_type(), array(
            'description' => $data["description"]
        ) );

        update_term_meta($post_id['term_id'], "icon", $data["icon_id"]);
		update_term_meta($post_id['term_id'], "color", $data["color"]);
        update_term_meta($post_id['term_id'], "order", $data["order"]);
		return $post_id;
	}
	
	

    static function get_category($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, "category");
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;
        $c['id']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['order']			= get_term_meta( $course->term_id, "order", true);
        $c['color']			= get_term_meta( $course->term_id, "color", true);
        $c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
        $c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
        return $c;
    }


    public static function api_action($type, $methods, $code, $pars, $user){

	    switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
                    Bio_Category::update($pars, $code);
                    $articles[]	= static::get_category( $code );
                    $update = 'success';
                }else{
                    $update = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) {
                    Bio_Category::delete($code);
                    $msg = __("Category removed succesfully", BIO);
                }else{
                    $msg = 'error';
                }
                break;
            case "create":
                if(is_numeric($code)) {
                   Bio_Category::update($pars, $code);
                   $cat = static::get_category( $code );
                   $msg = sprintf( __("Category «%s» updated succesfully", BIO), $cat['post_title'] );
				   $articles[]	= $cat;
                }else{
                    $class = Bio_Category::insert($pars);
                    $articles[]			= static::get_category($class);
                    $msg = __("Category inserted succesfully", BIO);
                }
                break;
            case "read":
            default:
                //$code = (int)$code;
                if(is_numeric($code))
				{
                    $c							= static::get_category( $code );
                    $c['count']			= Bio_Course::get_article_count($code);

                    $courses[] 		= $c;
                    $articles = [];

                    $all 	= Bio_Article::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "post_date", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        isset($pars['author'])			? $pars['author']		: -1,		// "",
                        [ "category" => $code ]
                    );
                    foreach($all as $p)
                    {
                        $a 					= Bio_Article::get_article( $p, false );
                        $articles[]			= $a;
                    }
                }				
				else
				{
                    $terms = get_terms( array(
                        'taxonomy'      => static::get_type(),
                        'orderby'       => 'meta_value_num',
                        'order'         => 'ASC',
						'hide_empty'   	=> false,
                        'fields'        => 'all',
						"meta_key"	   	=> "order",
                    ) );
                    foreach($terms as $c)
                    {
                        $courses[]	= static::get_category( $c );
                    }
                }
                break;
        }

        return Array (
            "category" => $courses,
            "articles" => $articles,
            "id" => $code,
            "msg" => $msg,
            "update"=> $update
        );
    }

	static function wp_dropdown($params=-1)
	{
		if(!is_array($params))
			$params = [];
		if($params['terms'])
		{
			$terms		=  $params['terms'];
		}
		else
		{
			$terms = get_terms( array(
				'taxonomy'      => static::get_type(), 
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'fields'        => 'all', 
			) );
		}
		$html		= "<select ";
		if($params['class'])
			$html	.= "class='".$params['class'] . "' ";
		if($params['style'])
			$html	.= "style='".$params['style'] . "' ";
		if($params['name'])
			$html	.= "name='".$params['name'] . "' ";
		if($params['id'])
			$html	.= "id='".$params['id'] . "' ";
		if($params['special'])
		{
			$pars  	= explode(",", $params['special']);
			$html	.= "$pars[0]='$pars[1]' ";
		}
		$html		.= " >";
		$zero 		= $params['select_none'] ? $params['select_none'] : "---";
			if(!$params['none_zero'])
				$html	.= "<option value='-1' selected>$zero</option>";			
			
		if(count($terms))
		{
			foreach($terms as $term)
			{
				$html	.= "
				<option " . selected($term->term_id, $params['selected'], 0) . " value='".$term->term_id."'>".
					$term->name .
				"</option>";
			}
		}
		$html		.= apply_filters("bio_biology_theme_last_dropdown", "", $params, $terms) . "
		</select>";
		return $html;
	}


}