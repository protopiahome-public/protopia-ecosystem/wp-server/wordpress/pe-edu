<?php

class Bio_REST_hooks
{
	static function init()
	{
		add_filter( "bio_left_menu_rest",	[__CLASS__, "bio_left_menu_rest"], 10, 1 );
		//add_filter( "bio_get_users",		[__CLASS__, "bio_get_users"], 10, 2 );
		add_filter( "bio_get_articles",		[__CLASS__, "bio_get_articles"], 10, 2 );
		add_action( "pre_get_posts",		[__CLASS__, "course_permissions"], 10, 1 );
		add_action( "edit_post",			[__CLASS__, "edit_post"], 10, 2 );
		add_filter( 'wp_new_user_notification_email', [__CLASS__, 'wp_new_user_notification_email'], 10, 3 );
	}
	static function wp_new_user_notification_email($wp_new_user_notification_email, $user, $blogname)
	{
		
		return $wp_new_user_notification_email;
	}
	static function bio_get_users($users, $params)
	{
		global $wpdb, $ggggg;
		if($params['current_bio_tests'] && $params['credits'] && is_array($params['credits']))
		{
			$query = "SELECT DISTINCT user_id 
			FROM `bio_result` 
			WHERE test_id=" . $params['current_bio_tests'] . " 
			AND credits>".$params['credits'][0]." AND credits<".$params['credits'][1].";";
			$results = $wpdb->get_results($query);
			$u = [];
			foreach($users as $user)
				foreach($results as $res)
					if($res->user_id == $user->ID)
						$u[] = $user;
			$ggggg = $users;
			$users = $u;
		}
		return $users;
	}
	static function bio_get_articles($articles, $pars)
	{
		$new_articles = [];
		if($pars['author'] == -2 )
		{
			$author_id 	= get_current_user_id();
			$courses 	= Bio_Course::get_all_per_author($author_id);
			$cid		= [];
			foreach($courses as $course)
			{
				$cid[]	= $course->term_id;
			}
			$new_articles = Bio_Article::get_all(
				-1,  
				-1, 
				0, 
				"date", 
				'DESC', 
				"", 
				"all", 
				"AND", 
				"",
				[ BIO_COURSE_TYPE => $cid ], 
				"OR", 
				"publish"
			);
			foreach($new_articles as $a)
			{
				$enbl = true;
				foreach($articles as $oa)
				{
					if($oa->ID == $a->ID)
					{
						$enbl = false;
						break;
					}
				}
				if( $enbl ) $articles[] = $a;
			}
		}
		return $articles;
	}
	static function edit_post ( $post_id, $post )
	{
		$caps = Bio_User::get_caps();
		switch($post->post_type)
		{
			case BIO_ARTICLE_TYPE:				
				break;
		}
		
	}
	static function bio_left_menu_rest( $menu_item  )
	{
		global $wpdb;
		$user_id = get_current_user_id();
		switch($menu_item->id)
		{
			case "lead_articles":
				if(Bio_User::is_user_roles(["editor", "adminstrator"]))
				{
					$query 	= "SELECT DISTINCT article_id FROM `" . $wpdb->prefix . "article_cat_user_requests`";
					$a		= count($wpdb->get_results($query));
					if($a) 	$menu_item->alert = [ "hint" =>__("requests add to Main page", BIO), "label" => $a ];
				}
				break;
			case "lead_events":				
				$a		= Bio_Event::get_requests_count();
				if($a) 	$menu_item->alert = [ "hint" =>__("events's requests", BIO), "label" => $a ];
				break;				
			case "lead_courses":				
				$a		= Bio_Course::get_requests_count();
				if($a) 	$menu_item->alert = [ "hint" =>__("courses's requests", BIO), "label" => $a ];
				break;
			case "lead_questions":
				//if(Bio_User::is_user_cap( BIO_QUESTION_CORRECT )) break;
				$a		=  Bio_Test::get_error_question_count();
				if($a) 	
					$menu_item->alert = [ "hint" =>__("Errors in Questions", BIO), "label" => $a ];
				break;

//		    case "lead_tests":
//            $a		= Bio_Test::get_requests_count();
//            if($a) 	$menu_item->alert = [ "hint" =>__("requests", BIO), "label" => $a ];
//            break;

            //количество которые cтатей на каторое он подписался
            case "favorites":
                $a		= Bio_User::get_favorites_count( $user_id );
                if($a)	$menu_item->success = [ "hint" =>__("Articles", BIO), "label" => $a ];
                break;
            //количество которые он прошел
            case "tests":
				require_once BIO_REAL_PATH . "class/Bio_User_Test.class.php";
				$a 		= count( Bio_Test_API::get_results_of_user( $user_id ) );
				if($a)	$menu_item->success = [ "hint" =>__("Tests", BIO), "label" => $a ];
				break;
            //количество будущих мероприятий на которые он подписан
		    case "events":
				$a 		= Bio_Event::get_all_mine_count();
				if($a)	$menu_item->success = [ "hint" =>__("Events", BIO), "label" => $a ];
				break;
            //количество на которые он подписан
		    case "courses":
				$a		= Bio_User::get_my_courses_count( $user_id );
				if($a) 	$menu_item->success = [ "hint" =>__("Courses", BIO), "label" => $a ];
				break;

		}
		return $menu_item;
	}
	
	static function course_permissions( $query )
	{
		global $rests_hook;
		if( $query->tax_query )
			if( $query->tax_query->queried_terms )
			{
				$course	= $query->tax_query->queried_terms[ BIO_COURSE_TYPE ];
				$courses	= [];
				foreach($course['terms'] as $c)
				{
					$p = 0;
					$i = 0;
					while( $parent_id = wp_get_term_taxonomy_parent_id( $c, BIO_COURSE_TYPE ) )
					{
						$p = $parent_id;
						if($i++ > 5) break;
					}
					
					if($p != $c)
					{
						$c = $pd;
						$locked = get_term_meta($c, "locked", true);
						if($locked)	array_push($course['terms'], $p);
					}
					else
						/*	*/
						$locked = get_term_meta($c, "locked", true);
					if( $locked )
					{
						$courses[] = $c;							
					}						
				}
				$diff	= array_diff( $courses, Bio_User::is_subscribe_courses($course['terms']) );
				//$rests_hook = [ Bio_User::is_subscribe_courses($course['terms']),  $diff ];
				if($diff)
				{
					foreach($diff as $cid)
					{
						$query->set( BIO_COURSE_TYPE, '-' . $cid );
					}
				}
			}
		return $query;		
	}
}