<?php
class Bio_Mailing_Group extends Bio_Conglomerath
{
	
	static function get_type()
	{
		return BIO_MAILING_GROUP_TYPE;
	}
	static function admin_menu()
	{
		
	}
	static function is_requested()
	{
		return false;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 3);
		add_action("admin_menu",				[ __CLASS__, "admin_menu"] );		
		parent::init(); 
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Mailing group", BIO), // Основное название типа записи
			'singular_name'      => __("Mailing group", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Mailing group", BIO), 
			'all_items' 		 => __('Mailing groups', BIO),
			'add_new_item'       => __("add Mailing group", BIO), 
			'edit_item'          => __("edit Mailing group", BIO), 
			'new_item'           => __("add Mailing group", BIO), 
			'view_item'          => __("see Mailing group", BIO), 
			'search_items'       => __("search Mailing group", BIO), 
			'not_found'          => __("no Mailing groups", BIO), 
			'not_found_in_trash' => __("no Mailing groups in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Mailing groups", BIO), 
		);
		$pt = register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 4.5,
				"taxonomies"		 => [ ],
				"menu_icon"			 => "dashicons-megaphone",
				'show_in_nav_menus'  => true,
				'supports'           => array('title'),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	
	function add_distination_users($users_array)
	{
		global $wpdb, $rest_log;
		$table = static::get_table_name();		
		if(!is_array($users_array))
			$users_array = [$users_array];
		$now	= time();
		$u		= [];
		foreach( $users_array as $user )
		{
			$uid 	= is_array($user) ? $user['ID'] : $user->ID;
			$u[]	= "(null, $uid, $this->id, NOW() )";
		}
		$query = "REPLACE INTO $table (ID, user_id, post_id, date ) VALUES " .implode(",", $u);
		$rest_log = $query;
		$wpdb->query($query);		
	}
	static function insert($data)
	{
        $ins 	= parent::insert($data);
		$ins->add_distination_users($data['users']);
		return $ins;
	}
	
	static function update($data, $id)
	{
        $ins 	= parent::update($data, $id);
		$ins->add_distination_users($data['users']);
		return $ins;
	}
	
	
    public static function get_post( $p, $is_full=false, $is_admin=false )
	{
		return static::get_mailing_group( $p, $is_full, $is_admin );
	}
	
    static function get_mailing_group( $p, $is_full=false, $is_admin=false )
    {
		global $rest_log;
        $art				= static::get_instance($p);
        $a					= [];
        $a['id']			= $art->id;
        $a['post_title']	= $art->get("post_title");		
		$a['users']			= [];
		$users 				= $art->get_users();
		foreach($users as $user)
		{
			$a['users'][]	= $user;
		}
        return $a;
    }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
        /*
type - "bio_event"
*/
        $mailing_groups	= [];
        switch($methods) {
            case "update":

                if(is_numeric($code)) 
				{

                }
				else
				{
                    $update = 'error';
                }

                break;
            case "delete":
                if(is_numeric($code)) 
				{
                    $update = 'error';
                }
				else
				{
                    $update = 'error';
                }
                break;
            case "create":
				if( $code>0 ) 
				{
                    static::update($pars, $code);
                    $art = get_post($code);
					$msg = sprintf( __("Mailing Group «%s» updated succesfully", BIO), $art->post_title ); 
					$mailing_groups[]	=  $art;
                }
				else
				{
                    $article 	= static::insert($pars);
                    $mailing_groups[]	= static::get_mailing_group( $article->body, true );
					$code 		= $article->id;
                    $msg 		= __("Mailing Group inserted succesfully", BIO);
                }
                break;
            case "read":
            default:
                if(is_numeric($code)) 
				{
                    $mailing_groups[]			= static::get_mailing_group( $code );
                }
				else
				{
                    $all 		= static::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset'] * $pars['numberposts']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "id", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "time", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND"		// "AND"
                    );
                    foreach($all as $p)
                    {
                        $a 					= static::get_mailing_group( $p );
                        $mailing_groups[]			= $a;
                    }
                    $update = 'success';
                }
                break;

        }
        return Array (
            "mailing_groups" => $mailing_groups,
            "id" => $code,
            "msg"=> $msg
        );
    }
}