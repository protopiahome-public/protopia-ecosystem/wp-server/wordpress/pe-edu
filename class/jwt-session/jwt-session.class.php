<?php

require_once(__DIR__ . "/vendor/autoload.php");

class JwtSession
{
	public static function sign($user_id, $key)
	{
		$headers = array(
			'alg' => 'HS256', //alg is required. see *Algorithms* section for supported algorithms
			'typ' => 'JWT'
		);

		// anything that json serializable
		$payload = array(
			'sub' => $user_id,
			'iat' => time()
		);

		$jws_class = new \Gamegos\JWS\JWS();
		return $jws_class->encode($headers, $payload, $key);
	}
	
	public static function verify($jws, $key)
	{
		$jws_class = new \Gamegos\JWS\JWS();

		return $jws_class->verify($jws, $key)["payload"]["sub"];
	}
}

//echo $jws = JwtSession::sign(24, "aabb");
//echo JwtSession::verify($jws, "aabb");