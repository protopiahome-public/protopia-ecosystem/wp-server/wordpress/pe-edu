<?php

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Bio_Event extends Bio_Conglomerath
{
	static function get_type()
	{
		return BIO_EVENT_TYPE;
	}
	static function is_requested()
	{
		return true;
	}
	static function init()
	{		
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		add_action('bio_after_activate',		[__CLASS__, 'bio_after_activate'] );
		add_action( 'before_delete_post', 		[__CLASS__, 'before_delete_post'] );
		add_action("pe_graphql_make_schema", 	[__CLASS__, "pe_graphql_make_schema"], 50);
		parent::init();
	}
	static function before_delete_post($post_id)
	{
		$post = get_post( $post_id );		
		if( $post && $post->post_type == static::get_type() )
		{		
			global $wpdb;
			$table = static::get_table_name();
			$query = "DELETE FROM $table WHERE post_id=$post_id;";
			//file_put_contents(BIO_REAL_PATH . "log.log", "\n" . $query);
			$wpdb->query( $query );		
			
			$table = static::get_table_required_name();
			$query = "DELETE FROM $table WHERE post_id=$post_id;";
			$wpdb->query( $query );
			
			$query = "DELETE FROM ". $wpdb->prefix."postmeta WHERE meta_key='event_id' AND meta_value='$post_id';";
			//file_put_contents(BIO_REAL_PATH . "log.log", "\n" . $query);
			$wpdb->query( $query );
		}
	}
	static function bio_after_activate($type)
	{
		if($type == static::get_type())
		{
			global $wpdb;
			$query = "ALTER TABLE `" . $wpdb->prefix . "user_bio_event_request` ADD `form_data` LONGTEXT NOT NULL AFTER `date`;";
			$wpdb->query($query);
			$query = "ALTER TABLE `" . $wpdb->prefix . "user_bio_event` ADD `form_data` LONGTEXT NOT NULL AFTER `date`;";
			$wpdb->query($query);
		}
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Event", BIO), // Основное название типа записи
			'singular_name'      => __("Event", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Event", BIO), 
			'all_items' 		 => __('Events', BIO),
			'add_new_item'       => __("add Event", BIO), 
			'edit_item'          => __("edit Event", BIO), 
			'new_item'           => __("add Event", BIO), 
			'view_item'          => __("see Event", BIO), 
			'search_items'       => __("search Event", BIO), 
			'not_found'          => __("no Events", BIO), 
			'not_found_in_trash' => __("no Events in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Events", BIO), 
		);
		register_post_type(
			BIO_EVENT_TYPE, 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				"taxonomies"		 => ["category"],
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array('title','editor','author','thumbnail','excerpt','comments'),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	static function add_views_column( $columns )
	{
		$columns = parent::add_views_column( $columns );
		$columns['author']	= __("Author");
		return $columns;
	}



    function get_admin_form()
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		ob_start();
		wp_editor($this->get('post_content'), "post_content");
		$editor 	= ob_get_contents();
		ob_end_clean();
		
		//$courses	= get_the_terms( $this->id, BIO_COURSE_TYPE ) ;
		//$course		=  is_wp_error($courses) ? -1 : $courses[0]->term_id;

		$icon_id	= get_post_thumbnail_id( $this->id );
		$html = "
		<div class='row justify-content-md-center' post_id='$this->id' command='bio_edit_article'>
			<ul class='nav nav-pills mb-3 justify-content-center text-center' id='pills-tab' role='tablist'>
				<li class='nav-item'>
					<a class='nav-link active' id='pills-home-tab' data-toggle='pill' href='#pills-home' role='tab' aria-controls='pills-home' aria-selected='true'>".
					  __("Settings" ).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' id='pills-profile-tab' data-toggle='pill' href='#pills-profile' role='tab' aria-controls='pills-profile' aria-selected='false'>".
					  __("Pupils", BIO).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' href='" . get_permalink( $this->id ) . "' >".
					  __("Go to", BIO).
					"</a>
				</li>
			</ul>
			<div class='tab-content col-12' id='pills-tabContent'>
				<div class='tab-pane fade show active' id='pills-home' role='tabpanel' aria-labelledby='pills-home-tab'>
					
					<div class='card'>
						<div class='card-body'>
							<div class='row'>
								<form class='cab_edit' >
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Title", BIO). "</subtitle>
									<div class='col-12'>
										<input type='text' class='form-control' name='post_title' value='".$this->get("post_title")."' />
									</div>		
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Content", BIO). "</subtitle>
									<div class='col-12'>".
										$editor.
									"</div>	
									<div class='spacer-10'></div>
									
									<subtitle class='col-12'>". __("Course", BIO). "</subtitle>
									<div class='col-12'>".
										Bio_Course::wp_dropdown([
											"class" 	=> "form-control",
											"name"		=> "course_id",
											"selected"	=> $course	
										]).
									"</div>	
									<div class='spacer-10'></div>	
																
									<div class='col-12'>
										<input type='submit' class='btn btn-primary' value='". __("Edit", BIO) . "' />		
									</div>		
								</form>
							</div>
						</div>
					</div>
				</div>
				
				<div class='tab-pane fade' id='pills-profile' role='tabpanel' aria-labelledby='pills-profile-tab'>
				
					<div class='card'>
						<div class='card-body'>".
							static::users_box_func( $this->id, false ) . 
						"</div>
					</div>
				</div>
						
			</div>";
		return $html;
	}
	function get_icon()
	{
		return get_the_post_thumbnail_url( $this->id ); 
	}
	function request_form()
	{
		
		$time 		= $this->get_meta("time");
		$test_id	= $this->get_meta("test_id");
		if(is_user_logged_in())
		{
			$user 	= wp_get_current_user();
			$name	= $user->display_name;
			$email	= $user->user_email;
			$phone	= get_user_meta($user->ID, "phone", true);
			$is_user= $this->is_users_member();
		}
		if($test_id)
		{
			$test 		= Bio_Test::get_instance($test_id);
			$test_draw	= "<div class='row bio_test_content' id='bio_test_content'>". $test->draw( ) ."</div>";
		}
		
		switch(true)
		{
			case static::is_users_member():
				$form = "<div class='text-center lead col-12' role='alert'>" . 
					Bio_Messages::get_you_event_member() .
				"<div>";
				break;
			case static::is_user_requested():
				$form = "<div class='text-center lead col-12' role='alert'>" . 
					Bio_Messages::get_you_event_requested() .
				"<div>";
				break;
			default:
				$form = "	
				<div class='col-12 bio_description'>" . 
					__( !$test_id ? 
						"To receive an invitation to the event, fill in all fields and click the 'Submit application' button." : 
						"To receive an invitation to the event, fill in all fields, take the test and click the 'Submit application' button.", 
					BIO) . 
				"</div>
				<div class='spacer-15'></div>			
				<form class='event_form col-12'>
					<div class='input-group mb-3'>
						<div class='input-group-prepend'>
							<span class='input-group-text'>
								<i class='fas fa-user'></i>
							</span>
						</div>
						<input type='text' name='user' class='form-control' placeholder='Username' aria-label='Username' value='$name' required>
					</div>
					
					<div class='input-group mb-3'>
						<div class='input-group-prepend'>
							<span class='input-group-text'>
								<i class='fas fa-at'></i>
							</span>
						</div>
						<input type='text' name='email' class='form-control' placeholder='Youe e-mail' aria-label='E-mail' value='$email' required>
					</div>
					
					<div class='input-group mb-3'>
						<div class='input-group-prepend'>
							<span class='input-group-text'>
								<i class='fas fa-phone'></i>
							</span>
						</div>
						<input name='phone' type='text' class='form-control' placeholder='Your phone' aria-label='E-mail' value='$phone' required>
					</div>
					
					$_test_draw
					
					<div class=''>
						<input type='submit' class='btn btn-primary' value='" .  __("Submit application", BIO) . "' />
					</div>
				</form>";
		}
		
		$html = $is_user ? 
		"<div class='row alert alert-success text-center'  role='alert'>
			<div class='col-12 '>
				<div class='bio_little_icon' style='background-image:url(" . $this->get_icon() . ");'></div>
			</div>
			<div class='col-12 lead'>".
				sprintf( __("You are already member of %s", BIO), "<b> ".$this->get("post_title")." </b>" ) .
			"</div>
			<div class='col-12'>
				<div class='col-12 bio_description'>" . sprintf( __("start: %s final: %s", BIO), "<b>". $time[0] ."</b>", "<b>".$time[1]."</b>" ) . "</div>
			</div>
		</div>" :
		"<div class='row request_form  text-center ' event_id='$this->id'>
			<div class='col-12 bio_title'>
				<div class='bio_little_icon' style='background-image:url(" . $this->get_icon() . ");'></div>" . 
				$this->get("post_title") . 
			"</div>
			<div class='col-12 bio_description'>" . sprintf( __("start: %s final: %s", BIO), "<b>". $time[0] ."</b>", "<b>".$time[1]."</b>" ) . "</div>
					
			$form
		</div>";
		return $html;
	}
	function draw()
	{
		$html = "--";
		return  $html;
	}
	function send_request($data) 
	{
		global $wpdb, $rest_log1;
		require_once ABSPATH . WPINC .'/registration.php';
		if( !is_user_logged_in() )
		{
			foreach( $data['form_data'] as $fd )
			{
				if($fd['type'] == "User")
				{
					extract( Bio_User::register_anonim( $fd['value'] ) ); 
					$user_id 	= $user->ID;
					break;
				}				
			}
			if( $user_id ) 
			{
				
			}
			else
				return false;
		}
		else
		{
			$user_id 	= get_current_user_id();
		}		
		$form_data		= serialize($data['form_data']);
		$table = static::get_table_name() . "_request";
		$query = "INSERT INTO `$table` (`ID`, `user_id`, `post_id`, `is_join`, `date`, `form_data`) VALUES (NULL, '$user_id', '$this->id', '1', CURRENT_TIMESTAMP, '$form_data');";
		$w = $wpdb->query( $query );		
		// wp_die( $query );		
		
		$this->update_meta( "request_count", $this->requests_count() );
		
		return $user;
	}
	function withdraw_permission($user_id)
	{
		global $wpdb;
		$table 		= static::get_table_required_name(); 
		$post_id	= $this->id;
		$query 		= "DELETE FROM $table WHERE post_id='$post_id' AND user_id='$user_id';";
		// wp_die( [$user_id, $post_id, $query] );
		$wpdb->query( $query );
		$table = static::get_table_name();
		$query 		= "DELETE FROM $table WHERE post_id='$post_id' AND user_id='$user_id';";
		// wp_die( [$user_id, $post_id, $query] );
		$wpdb->query( $query );
		 
		$this->update_meta( "request_count", $this->requests_count() );
	}
	static function get_requests_count()
	{
		global $wpdb;
		$in = Bio_User::is_user_roles(['administrator', 'editor']) 
			? "1=1" 
			: "post_id IN ( SELECT ID FROM ".$wpdb->prefix."posts WHERE post_author='" . get_current_user_id() . "'  )";
		$query = "SELECT COUNT(*) FROM ". Bio_Event::get_table_required_name() . " WHERE $in";
		return (int)$wpdb->get_var($query);
	}
	function get_request($user_id)
	{
		global $wpdb;
		$query = "SELECT COUNT(form_data) FROM `". Bio_Event::get_table_required_name() . "` WHERE post_id=".$this->id." AND user_id=$user_id";
		$re = $wpdb->get_var($query);
		//wp_die($query);
		if($re)
		{
			$res = unserialize( $re);
			$res->request = true;
			return $res;
		}
		else
		{
			$query = "SELECT COUNT(form_data) FROM `". Bio_Event::get_table_name() . "` WHERE post_id=".$this->id." AND user_id=$user_id";
			$re = $wpdb->get_var($query);
			// wp_die($query);
			$res = unserialize( $re);
			if($re)	$res->request = false;
			return $res;
		}
	}
	static function get_single_matrix($p)
	{
		$matrix  	= parent::get_single_matrix($p);
		$ins 		= static::get_instance($p); 
		$status		= $ins->get_request( get_current_user_id() ); 
		//wp_die($status);
		if($status != false)
		{
			$matrix['member_status'] = $status->request ? 1 : 2;
		}
		else
		{
			$matrix['member_status'] = 0;
		}
		//request_form
		$matrix['request_form'] = (string)json_encode ($status);
		$matrix['request_count'] = (int)$ins->get_meta("request_count");
		
		return $matrix;
	}
	
	static function get_all_mine( $user_id = -1)
	{
		$user_id = $user_id < 1 ? get_current_user_id() : $user_id;
		global $wpdb;
		$query1 = "SELECT p.ID, p.post_title, be.date FROM " . static::get_table_name() . " AS be
			LEFT JOIN " . $wpdb->prefix . "posts as p ON p.ID=be.post_id
			WHERE be.user_id = $user_id
			AND p.post_status='publish'";
		$usage =  $wpdb->get_results( $query1 );
		$query = "SELECT p.ID, p.post_title, be.date FROM " . static::get_table_required_name() . " AS be
			LEFT JOIN " . $wpdb->prefix . "posts as p ON p.ID=be.post_id
			WHERE be.user_id = $user_id
			AND p.post_status='publish'";
		$requests =  $wpdb->get_results( $query );
		return [ "requests" => $requests, "usage" => $usage, "query" => $query1 ];
	}
	static function get_all_mine_count( $user_id = -1 )
	{		
		$user_id = $user_id < 1 ? get_current_user_id() : $user_id;
		global $wpdb;
		$query1 = "SELECT COUNT(*) 
			FROM " . static::get_table_name() . " AS be
			LEFT JOIN " . $wpdb->prefix . "postmeta as p ON p.post_id=be.post_id
			WHERE be.user_id = $user_id
			AND p.meta_key='time' AND meta_value>". time().";";
		return  (int)$wpdb->get_var( $query1 );
	}

    public static function get_post( $p, $is_full=false, $is_admin=false )
	{
		return static::get_event( $p, $is_full, $is_admin );
	}
    static function get_event( $p, $is_full=false, $is_admin=false )
    {
		global $rest_log;
        $art				= Bio_Event::get_instance($p);
        $a					= [];
        $a['id']			= $art->id;
        $a['ID']			= $art->id;
        $a['post_title']	= $art->get("post_title");
        $a['post_content']	= $art->get("post_content");
        $a['request_form']	= $art->get_meta("request_form");
        $time				= get_post_meta($art->id, "time", true);
        $a['_time']			= $time;//strtotime( $time );
        $a['time']			= date_i18n('j F Y H:i', strtotime( $time ));
        $thumbnail			= get_the_post_thumbnail_url( $art->id, "full" );
        $a['thumbnail_id']	= (int)$art->id;
        $a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
        
		if($is_admin)
		{
			$a['requests']	= $art->get_requests( BIO_INTRAMURAL_TEST_ORDER );
			$a['members']	= $art->get_users( BIO_INTRAMURAL_TEST_ORDER );
			$a['query']		= $rest_log;
			$over 			= $art->get_my_over();
			$byUser			= Bio_Event::get_by_user( );
			$a['over']		= [];
			foreach($over as $e)
			{
				if( !in_array($e->ID, $byUser))
					$a['over'][] = static::get_event($e);
			}
		}
		// Тест
        $t					= [];
        $test_id = $art->get_meta("test_id");
        if( $test_id > 0)
        {
            $test			= Bio_Test::get_instance($test_id);
            $t['id']		= $test->id;
            $t['post_title']= $test->get("post_title");
        }
        $a['test']			= $t;

        // Курс
		$ccs				= [];
        $cours				= get_the_terms($art->id, BIO_COURSE_TYPE);
		foreach($cours as $cour)
		{
			array_push($ccs, Bio_Course::get_course($cour));
		}
        $a["course"]		= $ccs;
		
        // Bio_Biology_Theme::get_type()
		$ccs				= [];
        $cours				= get_the_terms($art->id, Bio_Biology_Theme::get_type());
		foreach($cours as $cour)
		{
			array_push($ccs, Bio_Biology_Theme::get_category($cour));
		}
        $a["bio_biology_theme"]	= $ccs;
		
        // Bio_Class::get_type()
		$ccs				= [];
        $cours				= get_the_terms($art->id, Bio_Class::get_type());
		foreach($cours as $cour)
		{
			array_push($ccs, Bio_Class::get_class($cour));
		}
        $a["bio_class"]	= $ccs;
		
        // Bio_Olimpiad_Type::get_type()
		$ccs				= [];
        $cours				= get_the_terms($art->id, Bio_Olimpiad_Type::get_type());
		foreach($cours as $cour)
		{
			array_push($ccs, Bio_Olimpiad_Type::get_category($cour));
		}
        $a["bio_olimpiad_type"]	= $ccs;
		
        // Bio_Role_Taxonomy::get_type()
		$ccs				= [];
        $cours				= get_the_terms($art->id, Bio_Role_Taxonomy::get_type());
		foreach($cours as $cour)
		{
			array_push($ccs, Bio_Role_Taxonomy::get_class($cour));
		}
        $a["bio_role_taxonomy"]	= $ccs;
		
		
		$a['is_user_requested']	= $art->is_user_requested();
		$a['is_users_member']	= $art->is_users_member();
		
        $user				= get_user_by("id", $art->get("post_author"));
        $auth				= [];
        $auth["id"]			= $user->ID;
        $auth["display_name"]= $user->display_name;
        $a['post_author']	= $auth;
        return $a;
    }
	function get_my_over()
	{
		return Bio_Event::get_all(
			[ "time" => [ "value" => time(), "compare" => ">" ] ],
			-1,
			0,
			null,
			null,
			null,
			"all",
			"AND",
			get_current_user_id()
		);
	}
    public static function api_action($type, $methods, $code, $pars=[], $user, $response)
    {
        /*
			type - "bio_event"
		*/
        $posts	= [];
        switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
					$msg = "methods: $methods!!!";
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_EVENT_DELETE, "Delete event");
                    wp_delete_post($code);
                    $msg = __("Event removed successfully", BIO);
                }
				else
				{
                    $msg = 'error';
                }
                break;
                break;
            case "create":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_EVENT_EDIT, "Update event");
					$p		= static::update($pars,$code);
					$posts[]= static::get_event( $code );
					$msg 	= sprintf( __("Event «%s» updated successfully", BIO), $p->get("post_title") ); 
                }
				else
				{
					Bio_User::access_caps(BIO_EVENT_CREATE, "Insert event");
					$p		= static::insert( $pars );
					$posts[]= static::get_event( $p->id );
					$msg 	= sprintf( __("Event «%s» insert successfully", BIO), $p->get("post_title") ); 
					$response->is_insert = true;
                }
                break;
            case "read":
            default:
                if(is_numeric($code)) 
				{
                    $posts[]			= static::get_event( $code, $pars['is_full'], $pars['is_admin'] );
                }
				else
				{					
					$author = $pars['author'] == -2 && !Bio_User::is_user_roles(['administrator', 'editor'])
						? get_current_user_id() 
						: $pars['author'];
					$all 		= Bio_Event::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset'] * $pars['numberposts']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "meta_value_num", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "time", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",
                        $author,
                        -1,		//
						"OR", 
						isset($pars['post_status'])		? $pars['post_status']	: "publish"// "AND"
                    );
                    foreach($all as $p)
                    {
                        $a 					= static::get_event( $p, $pars['is_full'], $pars['is_admin'] );
                        $posts[]			= $a;
                    }
                    $msg = null;
                }
                break;

        }
        return Array (
            BIO_EVENT_TYPE => $posts,
            "articles" 	=> [],
            "id" 		=> $code,
            "update"	=> $update,
			"msg"		=> $msg
        );
    }
	
	static function pe_graphql_make_schema()
	{
		PEGraphql::add_object_type( 
			[
				"name"			=> 'EventRequest',
				'description' 	=> __( "Request Pupil to Event", BIO ),
				'fields' 		=> [
					"user"		=> Type::listOf( PEGraphql::object_type( "User" ) ),
					"event"		=> Type::listOf( PEGraphql::object_type( "Bio_Event" ) )
				]
			]
		);
		PEGraphql::add_object_type( 
			[
				"name"			=> 'EventMember',
				'description' 	=> __( "Request Pupil to Event", BIO ),
				'fields' 		=> [
					"user"		=> Type::listOf( PEGraphql::object_type( "User" ) ),
					"event"		=> Type::listOf( PEGraphql::object_type( "Bio_Event" ) )
				]
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"			=> 'EventRequestInput',
				'description' 	=> __( "Request Pupil to Event", BIO ),
				'fields' 		=> [
					"user"		=> Type::int(), 
					"event"		=> Type::int(),
					"message"	=> Type::string()
				]
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"			=> 'EventRequestPaging',
				'description' 	=> __( "Request Pupil to Event paging", BIO ),
				'fields' 		=> [
					//"user"		=> Type::listOf( Type::int() ), 
					"event"		=> Type::listOf( Type::string() )
				]
			]
		);
		PEGraphql::add_query( 
			'getEventRequests', 
			[
				'description' 		=> __( "Get user's requests t events array", BIO ),
				'type' 				=> Type::listOf( PEGraphql::object_type("EventRequest") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("EventRequestPaging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					$users 	= [];
					$reqs 	= [];
					//wp_die($args);
					if(isset( $args['paging']) && isset( $args['paging']["event"] ) )
					{
						foreach( $args['paging']["event"] as $evt )
						{
							$inst 	= static::get_instance($evt);
							$users 	= $inst->get_requests();
							$_users = [];
							foreach($users as $user)
							{
								if($user->ID)
									$_users[] = Bio_User::get_user( $user->ID);
							}
							$reqs[]	= [
								"user" 	=> $_users,
								"event"	=> [ static::get_single_matrix( $evt ) ]
							];
						}						
					}
					return $reqs;
				}
			] 
		);
		PEGraphql::add_query( 
			'getEventMembers', 
			[
				'description' 		=> __( "Get user's requests events array", BIO ),
				'type' 				=> Type::listOf( PEGraphql::object_type("EventMember") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("EventRequestPaging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					$users 	= [];
					$reqs 	= [];
					//wp_die($args);
					if(isset( $args['paging']) && isset( $args['paging']["event"] ) )
					{
						foreach( $args['paging']["event"] as $evt )
						{
							$inst 	= static::get_instance($evt);
							$users 	= $inst->get_users();
							$_users = [];
							foreach($users as $user)
							{
								if($user->ID)
									$_users[] = Bio_User::get_user( $user->ID);
							}
							$reqs[]	= [
								"user" 	=> $_users,
								"event"	=> [ static::get_single_matrix( $evt ) ]
							];
						}						
					}
					return $reqs;
				}
			] 
		);
		PEGraphql::add_query( 
			'getFullEventMembersCount', 
			[
				'description' 		=> __( "Get user's requests events array", BIO ),
				'type' 				=> Type::int(),
				'args'     			=> [ ],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					return static::get_requests_count();
				}
			] 
		);
		PEGraphql::add_mutation ( 
			'Apply_Request', 
			[
				'description' 	=> __( "Apply request to Event by Pupil.", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type("EventRequestInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) 
				{	
					//wp_die($args);
					$data = [
						"user" 		=> $args['input']["user"],
						"form_data"	=> $args['input']["message"] + "111"
					];
					$ins = static::get_instance($args['input']["event"]);
					$ins->send_request($data) ;
					return true;
				}
			] 
		);
		
		PEGraphql::add_mutation ( 
			'Withdraw_Request', 
			[
				'description' 	=> __( "Withdraw request to Event by Pupil.", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type("EventRequestInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) 
				{	
					//wp_die($args);
					$ins = static::get_instance($args['input']["event"]);
					$ins->withdraw_permission($args['input']["user"]);
					return true;
				}
			] 
		);
		PEGraphql::add_mutation ( 
			'Accept_Request', 
			[
				'description' 	=> __( "Accept request from Event.", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type("EventRequestInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) 
				{	
					// wp_die( $args['input'] );
					// $ins = static::get_instance($args['input']["event"]);
					static::set_access_request($args['input']["event"], $args['input']["user"]);
					return true;
				}
			] 
		);
		PEGraphql::add_mutation ( 
			'Refuse_Request', 
			[
				'description' 	=> __( "Refuse request from Event.", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type("EventRequestInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) 
				{	
					// wp_die( $args['input'] );
					// $ins = static::get_instance($args['input']["event"]);
					static::refuse_permission($args['input']["event"], $args['input']["user"]);
					return true;
				}
			] 
		);
	}
	
	
	
	static function insert($data)
	{
		//$data['request_form'] = serialize($data['request_form']);		
		return parent::insert($data);
	}
	static function update($data, $id)
	{
		wp_delete_object_term_relationships($id, [
			Bio_Course::get_type(), 
			Bio_Biology_Theme::get_type(), 
			Bio_Class::get_type(), 
			Bio_Olimpiad_Type::get_type(),
			Bio_Role_Taxonomy::get_type()
		]);
		if( $data[Bio_Biology_Theme::get_type()] )
		{
			if(is_array($data[Bio_Biology_Theme::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Biology_Theme::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Biology_Theme::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Biology_Theme::get_type()
			);
			unset ($data[Bio_Biology_Theme::get_type()]);
		}
		
		if( $data['course'] )
		{
			if(is_array($data['course']))
			{
				$terms = [];
				foreach($data['course'] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data['course']];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Course::get_type()
			);
			unset ($data['course']);
		}
		
		if( $data[Bio_Olimpiad_Type::get_type()] )
		{
			if(is_array($data[Bio_Olimpiad_Type::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Olimpiad_Type::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Olimpiad_Type::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Olimpiad_Type::get_type()
			);
			unset ($data[Bio_Olimpiad_Type::get_type()]);
		}
		
		if( $data[Bio_Class::get_type()] )
		{
			if(is_array($data[Bio_Class::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Class::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Class::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Class::get_type()
			);
			unset ($data[Bio_Class::get_type()]);
		}
		
		if( $data[Bio_Role_Taxonomy::get_type()] )
		{
			if(is_array($data[Bio_Role_Taxonomy::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Role_Taxonomy::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Role_Taxonomy::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Role_Taxonomy::get_type()
			);
			unset ($data[Bio_Role_Taxonomy::get_type()]);
		}
		
		return parent::update($data, $id);
	}
}