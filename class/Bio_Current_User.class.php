<?php
class Bio_Current_User
{
    static function get_type()
    {
        return "";
    }

    static function init()
    {

    }

    public static function api_action($type, $methods, $code, $pars, $user){
        $user_id = $user->ID;
        $users	= [];
        switch($methods) {
            case "update":
				if(!$pars['id'] || ($pars['id'] && $user->ID == $pars['id']))
					Bio_User::update($pars, $user->ID);
				else if($pars['id'] && current_user_can("manage_options"))
				{
					Bio_User::update($pars, $pars['id']);
					$user_id = $pars['id'];
				}
                break;
            case "delete":
                if(is_numeric($code)) 
				{
                    $update = 'error';
                }
				else
				{
                    $update = 'error';
                }
                break;
            case "create":
                if($user_id)
				{
                    if($pars['id']=="" || ($pars['id'] && $user->ID == $pars['id']))
						Bio_User::update($pars, $user->ID);
					else if($pars['id'] && current_user_can("manage_options"))
					{
						Bio_User::update($pars, $pars['id']);
						$user_id = $pars['id'];
					}
					$msg = __( "Successful update User.", BIO );
                }
				else
				{
                    $answ = Bio_User::insert($pars);
                    if(is_wp_error($answ))
                    {
						/*
							ERRORS CODES:
							
							invalid_user_id
							empty_user_login
							user_login_too_long
							existing_user_login
							invalid_username
							user_nicename_too_long
							existing_user_email
						*/
                        $msg = $answ->get_error_codes();//$answ->get_error_message();
                    }
                    else
                    {
                        $login		= $pars['email'];
                        $passw		= $pars['psw'];

                        $msg 		= __("Insert new User succesfuly.", BIO);
                        $user			= Bio_REST::auth( $login, $passw );
                        if( $user )
                        {
							$user_id	= $user->ID;
                            $user_data 	= ["ID" => $user_id, "display_name"=> $user->display_name, "__typename" => "User" ];
							
							$code 		= md5(time());
                            $string 	= array('id' => $user_id, 'code' => $code);
							update_user_meta($user_id, 'account_activated', 0);
							update_user_meta($user_id, 'activation_code', $code);
							
							//$token= session_id();
                            //$msg	.= sprintf( __( "Successful logged in by %s", BIO ), $user->display_name );
                            Bio_Mailing::send_mail(								
                                sprintf( __("You are create account on pe-edu.ru", BIO),  $user->name ),
								sprintf( __("To ensure full functionality, please confirm your email address. To do this, follow <a href='%s'>this link</a>", BIO),  $pars['url'] . "/verify/$user_id/$code" ),
                                1,
                                [ $pars['email'] ]
                            );
                        }
                        else
                        {
                            $user_data = [ "ID" => -1, "display_name"=> __("Unlogged User", BIO), "__typename" => "User"  ];
                            $msg	= __( "User register but not logged inn. Unknown error :)", BIO );
                        }
                    }
                }
                break;
            case "read":
            default:
				//$msg	= __( "User register but not logged inn. Unknown error :)", BIO );
                break;
        }

        $users[] = Bio_User::to_rest($user_id);

        return Array (
            "users" => $users,
            "user" => $user_data,
            "id" => $code,
            "update"=> $update,
            "msg" => $msg,
            "token" => $token
        );

        //$response->pars2		= Bio_Article::$args;
    }
	
	static function request_verify( $pars )
	{
		$user_id	= get_current_user_id();
		$user 		= wp_get_current_user();
		$code 		= get_user_meta($user_id, 'activation_code', true);
		$code 		= !$code ? "11": $code;
		Bio_Mailing::send_mail(								
			sprintf( __("You are requested verify your account on pe-edu.ru", BIO),  $code ),
			sprintf( __("To ensure full functionality, please confirm your email address. To do this, follow <a href='%s'>this link</a>", BIO),  $pars['url'] . "/verify/$user_id/$code" ),
			1,
			[ $user->user_email ]
		);
	}

}
