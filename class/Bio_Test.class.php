<?php

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


//Это рабоатет с внешкой тестов
class Bio_Test extends Bio_Conglomerath
{
	static $test_edit;
	static function get_type()
	{
		return BIO_TEST_TYPE;
	}
	static function test_types()
	{
		return [
			[
				"post_title" 	=> "Standart",
				"id"			=> 1
			],
			[
				"post_title" 	=> "Course Electronic",
				"id"			=> 2
			],
			[
				"post_title" 	=> "Course Intramural",
				"id"			=> 3
			]
		];
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		add_action('admin_menu',				[ __CLASS__, 'my_extra_fields2' ]);
		add_filter('bio_gq_get_fields',			[ __CLASS__, 'bio_gq_get_fields' ],10, 4 );
		add_filter('peql_array_special',		[ __CLASS__, 'peql_array_special' ],10, 4 );
		add_action("pe_graphql_make_schema", 	[ __CLASS__, "exec_pegql"], 7);
		add_action("peql_array_spesial_resolve",[ __CLASS__, "peql_array_spesial_resolve"], 11, 10);
		
		parent::init();
		
	}
	static function activate( )
	{
		parent::activate( );
		global $wpdb;
		$query = "ALTER TABLE `" . $wpdb->prefix . "user_" . static::get_type() . "` ADD `rating` FLOAT(12) UNSIGNED NOT NULL DEFAULT '0' AFTER `date`;";
		$wpdb->query($query);
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Test", BIO), // Основное название типа записи
			'singular_name'      => __("Test", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Test", BIO), 
			'all_items' 		 => __('Tests', BIO),
			'add_new_item'       => __("add Test", BIO), 
			'edit_item'          => __("edit Test", BIO), 
			'new_item'           => __("add Test", BIO), 
			'view_item'          => __("see Test", BIO), 
			'search_items'       => __("search Test", BIO), 
			'not_found'          => __("no Tests", BIO), 
			'not_found_in_trash' => __("no Tests in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Tests", BIO), 
		);
		register_post_type(
			BIO_TEST_TYPE, 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array('title','editor', 'author'),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	
	static function get_test_name($test_id)
	{
		$test = static::get_instance( $test_id );
		return $test->get("post_title");
	}
	static function is_test_export_xml($test_id)
	{
		return get_post_meta( $test_id, "publish_type", true ) == 1;
	}
	
	static function fill_views_column($column_name, $post_id) 
	{	
		switch($column_name)
		{
			case "publish_type":
				$publish_type = get_post_meta($post_id, "publish_type", true);
				foreach(static::test_types() as $tp)
				{
					if($publish_type == $tp['id'])
					{
						echo __($tp['post_title'], BIO);
						break(2);
					}
				}
				echo $publish_type;
				break;
			default:
				parent::fill_views_column($column_name, $post_id);
		}
	}
	static function my_extra_fields2() 
	{
		add_meta_box( 'sel_type_fields', __('Select test type', BIO), [__CLASS__, 'extra_fields_box_func2'], static::get_type(), 'side', 'low'  );
		
	}
	static function extra_fields_box_func2( $post )
	{	
		$sel = (int)get_post_meta($post->ID, "test_type", true);
		echo "<div class='row'>
			<div class='col-12'>
				<input type='radio' class='radio' name='test_type' id='test_type_0' " . checked(0, $sel, 0) . " /> 
				<label for='test_type_0'>" . __("Choose single", BIO) . "</label>
			</div>
			<div class='col-12'>
				<input type='radio' class='radio' name='test_type' id='test_type_1' " . checked(1, $sel, 0) . " /> 
				<label for='test_type_1'>" . __("Choose several", BIO) . "</label>
			</div>
			<div class='col-12'>
				<input type='radio' class='radio' name='test_type' id='test_type_2' " . checked(1, $sel, 0) . " /> 
				<label for='test_type_2'>" . __("Choose all suitable", BIO) . "</label>
			</div>
		</div>";
	}

	function send_error_test($data)
	{
		global $wpdb;
		$query = "INSERT 
		INTO `".$wpdb->prefix."error_test` 
		(`ID`, `user_id`, `test_id`, `question_id`, `comment`, `date` )
		VALUES (NULL, '".get_current_user_id()."', '$this->id', '" . $data['question_id'] . "', '" . $data['comment'] . "', CURRENT_TIMESTAMP );";
		$wpdb->query( $query );
		$test_editors = get_users([
			'role__in'     => ['administrator','editor','TestEditor']
		]);
		$adressees=[];
		foreach($test_editors as $u)
		{
			$adressees[] = $u->data->user_email ;
		}
		Bio_Mailing::send_mail( 
			sprintf(__(BIO_ERROR_TEST_TITLE, BIO), __(BIO_ERROR_TEST_MESSAGE, BIO) . " " . $this->get("post_title")), 
			$data['comment'], 
			get_current_user_id(), 
			$adressees 
		);
		return $adressees;
	}
	function get_error_test()
	{
		global $wpdb;
		$query = "SELECT * FROM ".$wpdb->prefix."error_test WHERE test_id=".$this->id;
		return $wpdb->get_results( $query );
	}
	function remove_error_test( $error_id)
	{
		global $wpdb;
		$query = "DELETE FROM ".$wpdb->prefix."error_test WHERE ID=".$error_id;
		return $wpdb->query( $query );
	}
	static function get_error_question_count()
	{
		global $wpdb;
		$query = "SELECT COUNT( DISTINCT question_id ) FROM ".$wpdb->prefix."error_test";
		return $wpdb->get_var( $query );
	}
	static function update($data, $id)
	{
		if( $data['thumbnail_id'] < 1 )
        {
            $media = Bio_Assistants::insert_media([
                "data" => $data['thumbnail'],
                "media_name"=> $data['media_name']
                ], $id);
			wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
            $data['thumbnail_id']	= $media['id'];
            $data['thumbnail']		= $media['url'];
        }

		if( $data["thumbnail_id"] > 0 )
		{
			set_post_thumbnail(
				$id,
				(int) $data["thumbnail_id"]
			);
		}

        unset ($data["thumbnail_id"]);
        unset ($data["thumbnail"]);
		wp_delete_object_term_relationships($id, [
			Bio_Course::get_type(), 
			Bio_Biology_Theme::get_type(), 
			Bio_Class::get_type(), 
			Bio_Category::get_type(), 
			Bio_Test_Category::get_type(),
			Bio_Olimpiad_Type::get_type(),
			Bio_Role_Taxonomy::get_type()
		]);
		if( $data["bio_course"] )
		{
			$terms = [];
			foreach($data["bio_course"] as $term)
			{
				$terms[] = (int)$term;
			}
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Course::get_type()
			);
			unset ($data["bio_course"]);
		}
		if( $data[Bio_Biology_Theme::get_type()] )
		{
			if(is_array($data[Bio_Biology_Theme::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Biology_Theme::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Biology_Theme::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Biology_Theme::get_type()
			);
			unset ($data[Bio_Biology_Theme::get_type()]);
		}
		if( $data[Bio_Category::get_type()] )
		{
			if(is_array($data[Bio_Category::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Category::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Category::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Category::get_type()
			);
			unset ($data[Bio_Category::get_type()]);
		}
		if( $data[Bio_Olimpiad_Type::get_type()] )
		{
			if(is_array($data[Bio_Olimpiad_Type::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Olimpiad_Type::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Olimpiad_Type::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Olimpiad_Type::get_type()
			);
			unset ($data[Bio_Olimpiad_Type::get_type()]);
		}
		if( $data[Bio_Test_Category::get_type()] )
		{
			if(is_array($data[Bio_Test_Category::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Test_Category::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Test_Category::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Test_Category::get_type()
			);
			unset ($data[Bio_Test_Category::get_type()]);
		}
		if( $data[Bio_Class::get_type()] )
		{
			if(is_array($data[Bio_Class::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Class::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Class::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Class::get_type()
			);
			unset ($data[Bio_Class::get_type()]);
		}
		if(isset($data['questions']) && is_array($data['questions']))
		{
			static::$test_edit = Bio_Test_API::test_edit(
				$id, 
				-1, //$data['added_questions'],  
				-1, //$questions_edit, 			
				-1, //$data['delete_questions'],  
				$data['questions']  //$questions
			);
		}
		return parent::update($data, $id);
	}
	static function insert( $data )
	{
		$inst 	= parent::insert($data);
		$id		= $inst->id;
		if( $data['thumbnail_id'] < 1 )
        {
            $media = Bio_Assistants::insert_media([
                "data" => $data['thumbnail'],
                "media_name"=> $data['media_name']
                ], $id);
			wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
            $data['thumbnail_id']	= $media['id'];
            $data['thumbnail']		= $media['url'];
        }

		if( $data["thumbnail_id"] > 0 )
		{
			set_post_thumbnail(
				$id,
				(int) $data["thumbnail_id"]
			);
		}
        unset ($data["thumbnail_id"]);
        unset ($data["thumbnail"]);
		wp_delete_object_term_relationships($id, [
			Bio_Course::get_type(), 
			Bio_Biology_Theme::get_type(), 
			Bio_Class::get_type(), 
			Bio_Category::get_type(), 
			Bio_Test_Category::get_type(),
			Bio_Olimpiad_Type::get_type(),
			Bio_Role_Taxonomy::get_type()
		]);
		if( $data["bio_course"] )
		{
			$terms = [];
			foreach($data["bio_course"] as $term)
			{
				$terms[] = (int)$term;
			}
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Course::get_type()
			);
			unset ($data["bio_course"]);
		}
		if( $data[Bio_Biology_Theme::get_type()] )
		{
			if(is_array($data[Bio_Biology_Theme::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Biology_Theme::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Biology_Theme::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Biology_Theme::get_type()
			);
			unset ($data[Bio_Biology_Theme::get_type()]);
		}
		if( $data[Bio_Category::get_type()] )
		{
			if(is_array($data[Bio_Category::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Category::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Category::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Category::get_type()
			);
			unset ($data[Bio_Category::get_type()]);
		}
		if( $data[Bio_Olimpiad_Type::get_type()] )
		{
			if(is_array($data[Bio_Olimpiad_Type::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Olimpiad_Type::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Olimpiad_Type::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Olimpiad_Type::get_type()
			);
			unset ($data[Bio_Olimpiad_Type::get_type()]);
		}
		if( $data[Bio_Test_Category::get_type()] )
		{
			if(is_array($data[Bio_Test_Category::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Test_Category::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Test_Category::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Test_Category::get_type()
			);
			unset ($data[Bio_Test_Category::get_type()]);
		}
		if( $data[Bio_Class::get_type()] )
		{
			if(is_array($data[Bio_Class::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Class::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Class::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Class::get_type()
			);
			unset ($data[Bio_Class::get_type()]);
		}
		static::$test_edit = Bio_Test_API::test_add(
			$id, 
			$data['questions']  //$questions
		);
	}
    static function get_test( $p, $is_full = false )
    {
        $id = is_numeric($p) ? $p : $p->ID;
        //Bio_Test_API::test_refresh( $id );
        return static::make_test($p, $is_full);
        //return static::make_test($p);
    }

    static function make_test( $p, $is_full = false, $is_admin=false )
    {
        //$result = self::memcached()->get("bio_rest|test|{$p}");
        //if (false && !$result)
        //{
		$try_count_exhausted = false;
        $art				= Bio_Test::get_instance($p);
		$try_count			= (int)$art->get_meta("try_count");		
		$publish_type		= (int)$art->get_meta("publish_type");
		if (get_current_user_id() && $is_full)
		{
			$try_count_used 		= (int)Bio_Test_API::get_count_results_of_user($art->id, get_current_user_id());
			$try_count_exhausted 	= $try_count > 0 ? $try_count_used <= $try_count : true;				
		}
		$a					= ( $is_full && $try_count_exhausted ) || $is_admin 
			? Bio_Test_API::test_get( $art->id ) 
			: [ "questions" => [] ];
        /*
		$a = Bio_Test_API::test_get( $art->id ) ;
		*/
        $a['id']			= (int)$art->id;
        $a['post_title']	= $art->get("post_title");
        $a['post_date']		= date_i18n( 'j F Y', strtotime( $art->get("post_date") ) );
		$a['try_count']		= $try_count;
		$a['publish_type']	= $publish_type;		
		$a['try_count_exhausted'] = $try_count_exhausted;
		$a["try_count_used"] = $try_count_used;
        $a['is_timed']		= (int)$art->get_meta("is_timed");
        $a['is_shuffle']	= (int)$art->get_meta("is_shuffle");
        $a['is_show_results'] = (int)$art->get_meta("is_show_results");
        $a['is_group']		= (int)$art->get_meta("is_group");
        $a['duration']		= (int)$art->get_meta("duration");
		
        if($is_admin)
        {
            $a['members']	= [];
        }
		
        // Курс
        $ccs				= [];
        $cours				= get_the_terms($art->id, BIO_COURSE_TYPE);
		if( $cours !== false && !is_wp_error($cours) )
		{
			foreach($cours as $cour)
			{
				array_push($ccs, Bio_Course::get_course($cour));
			}
		}
        $a["course"]		= $ccs;

        // Тип Олимпиад
        $ot				= [];
        $cours	= get_the_terms($art->id, BIO_OLIMPIAD_TYPE_TYPE);
        if( $cours !== false && !is_wp_error($cours) )
		{
			foreach($cours as $cour)
			{
				array_push($ot, Bio_Olimpiad_Type::get_category($cour));
			}
		}
        $a[BIO_OLIMPIAD_TYPE_TYPE]		= $ot;

        // Класс
        $classes			= [];
        $cours	= get_the_terms($art->id, BIO_CLASS_TYPE);
        if( $cours !== false && !is_wp_error($cours) )
		{
			foreach($cours as $cour)
			{
				array_push($classes, Bio_Class::get_class($cour));
			}
		}
        $a["_class"]		= $classes;
        /**/


        // Темы
        $bio_theme			= [];
        $bt	= get_the_terms( $art->id, BIO_BIOLOGY_THEME_TYPE );
		if( $bt !== false && !is_wp_error($bt) )
		{
			foreach($bt as $cour)
			{
				array_push($bio_theme, Bio_Biology_Theme::get_category($cour));
			}
		}
        $a[BIO_BIOLOGY_THEME_TYPE]		= $bio_theme;
		
        // Категория тестов
        $test_tax	= [];
		$bt			= get_the_terms( $art->id, BIO_TEST_CATEGORY_TYPE );
		if( $bt !== false && !is_wp_error($bt) )
		{
			foreach($bt as $cour)
			{
				array_push($test_tax, Bio_Test_Category::get_category($cour));
			}
		}
        $a[BIO_TEST_CATEGORY_TYPE] = $test_tax;
        /**/

        $user				= get_user_by("id", $art->get("post_author"));
        $auth				= [];
        $auth["id"]			= $user->ID;
        $auth["display_name"]= $user->display_name;
        $a['post_author']	= $auth;

        /**/

        $result = $a;
        //}
        //self::memcached()->set("bio_rest|test|{$p}", $result);
        return $result;
    }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
        /*
			type - "bio_test"
		*/
		global $boo;
        $np 	= 20;
        $tests	= [];
        switch($methods) 
		{
            case "update":

                if(is_numeric($code)) 
				{

                }
				else
				{
                    $update = 'error';
                }

                break;
            case "delete":
                if(is_numeric($code)) 
				{
                    $msg = 'error';
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "create":
				 if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_TEST_EDIT, "Update test");
					$p			= static::update($pars, $code);
					$test		= Bio_Test::get_instance($code);
					$tests[]	= static::get_test( $code );
					$msg 		= sprintf( __("Test «%s» updated successfully", BIO), wp_strip_all_tags($test->get("post_title")) ) . ". "; 
					$msg		.= __( static::$test_edit ? "Questions updates." : "Unknown error: Questions NOT updated!");
                }
				else
				{
					Bio_User::access_caps(BIO_TEST_CREATE, "Insert test");
					$p		= static::insert( $pars );
					$tests[]= static::get_test( $p->id );
					$msg 	= sprintf( __("Test «%s» insert successfully", BIO), wp_strip_all_tags( $p->get("post_title" ))); 
					$response->is_insert = true;
                }
                break;
            case "read":
            default:
                if(is_numeric($code)) 
				{
                    $t				= Bio_Test::get_instance($code);
                    $test			= static::get_test( $code, true );
					$tests[]		= $test;
                    /*
					$questions		= $t->get_questions();
                    $q = [];
                    foreach($questions as $que)
                    {
                        $q[] 	= Bio_Question::get_question($que);
                    }
					*/
                    break;
                }
				else
				{			
					$author = $pars['author'] == -2 && !Bio_User::is_user_roles(['administrator', 'editor'])
						? get_current_user_id() 
						: $pars['author'];
					
					$metas = [ "publish_type" ];
					$m = [];
					
					foreach($metas as $meta)
					{
						if( $pars[$meta] > 0 )
							//$m[$meta] = $pars[$meta];
							$m[$meta] = is_array($pars[$meta]) 
								? [
									"value" 	=> $pars[$meta], 
									"operator" => "OR", 
									"type" 		=> "NUMERIC", 
									"compare" 	=> "IN" 
								] 
								: $pars[$meta];
					}
					
                    $all 		= Bio_Test::get_all(
						$m // isset($pars['metas']) 			? $pars['metas'] 		: [] 
                        ,$np  	// -1
                        ,isset($pars['offset'])			? $pars['offset'] * $np : 0  		// 0
                        ,isset($pars['order_by'])		? $pars['order_by']		: "title" 	// 'title'
                        ,isset($pars['order'])			? $pars['order']		: 'DESC' 	// 'DESC'
                        ,isset($pars['order_by_meta'])	? $pars['order_by_meta']: "" 		// ""
                        ,"all" 																// $pars['fields'],
                        ,isset($pars['relation'])		? $pars['relation']		: "OR"	// "AND"
						, $author 
						,isset($pars['taxonomies'])		? $pars['taxonomies']	: -1

                    );
                    foreach($all as $p)
                    {
                        $a 					= static::get_test( $p, false );
                        $tests[]			= $a;
                    }
					/**/
					$count = (int) count(static::get_all (
						$m //  isset($pars['metas']) 			? $pars['metas'] 		: [] 		// []
						,-1
						, 0  		// 0
						,isset($pars['order_by'])		? $pars['order_by']		: "title" 	// 'title'
						,isset($pars['order'])			? $pars['order']		: 'DESC' 	// 'DESC'
						,isset($pars['order_by_meta'])	? $pars['order_by_meta']: "" 		// ""
						,"ids" 																// $pars['fields'],
						,isset($pars['relation'])		? $pars['relation']		: "AND"	// "AND"
						, $author		// "",
						,isset($pars['taxonomies'])		? $pars['taxonomies']	: -1		// ["bio_course" => 5, "category" => 4]
					));					
                }
                break;

        }

        return Array (
            "tests" => $tests,
            "count" => $count,
            "offset" => (int)$pars['offset'],
            "numberposts" => $np,
            "articles" => [],
            "id" => $code,
            "msg" => $msg,
            "update"=> $update
        );

    }

    function get_count_questions()
	{
		global $wpdb;
		$query = "
		SELECT COUNT(*) FROM `" . $wpdb->prefix . "posts` AS p
		LEFT JOIN " . $wpdb->prefix . "postmeta AS pm ON pm.post_id=p.ID
		WHERE pm.meta_key='test_id' AND pm.meta_value=35
		AND p.post_status='publish'
		AND p.post_type='bio_question';";
		return $wpdb->get_var($query);
	}
	function get_questions()
	{
		return Bio_Question::get_all([ "test_id" => $this->id ]);
	}
	function get_question( $order = 1 )
	{
		return Bio_Question::get_all([ "test_id" => $this->id, "order" => $order ])[0];
	}
	function get_article()
	{
		return Bio_Article::get_all([ "test_id" => $this->id ])[0];
	}
	function get_article_title()
	{
		if($article = $this->get_article())
		{
			return $article->post_title;
		}
		else
			return "--";
	}
	function draw( $order = 0 )
	{
		$question 	= $this->get_question( $order );
		if($is_timed	= $this->get_meta("is_timed"))
		{
			$duration	= $this->get_meta("duration");
			$dt			= " test_dur='$duration' ";
		}
		if($order == 0)
		{
			$html = "
			<div class='wobble_2 col-12' style=' align-items: center; justify-content: center; display: flex; flex-direction: column;'>
				<div class='display-3'>" .
					__("Hallo. Start test", BIO) .
				"</div>
				<div  class='btn btn-success bio_test_next no' test_id='$this->id' current='$q->id' $dt >" .
					__("Start", BIO) .
				"</div >
			</div>";
			return $html;
		}
		else if( $question )
		{
			$q = Bio_Question::get_instance( $question );
			$html = "
			<div class='wobble_2 col-12'>";
			$html .= $q->draw();
			$html .= "
			<div class='row'>
				<div class='col-12 bio_meta'>
					<div class='spacer-30'></div>";
			for($i=1; $i<$this->get_count_questions()+1; $i++)
			{
				if($i == $order)
				{
					$html .= "
					<div  class='btn btn-success bio_test_next' test_id='$this->id' current='$q->id'>" .
						__("Choose and next", BIO) .
					"</div >";
				}
				else
				{					
					$html .= "
					<div type='button' class='btn btn-outline-secondary disabled' disabled>" .
						( $i ) .
					"</div >";
				}
			}
			$html .="
				
			</div></div>";
			return $html;
		}
		else
			return __("No Questions in this Test.", BIO);
	}
	function get_admin_questions_form()
	{
		$questions = Bio_Question::get_all(["test_id" => $this->id]);//, -1, 0, 'title' 'ASC', "order");
		$i = 0;
		$html 		= "
		<subtitle>". __("Questions:", BIO). "</subtitle>
		<div class='col-12'>
			<div class='row test_question_cont'>";
		foreach($questions as $quest)
		{
			$question  	= Bio_Question::get_instance($quest);
			$html		.= "
			<div class='admin_test_question_form' admin_question_id='$i'>".
				$question->get_admin_form2().
			"</div>";
			$i++;
		}
		$html 		.= "
			</div>
		</div>";
		return $html;
		/**/
	}
	function get_admin_form()
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		
		$questions = $this->get_admin_questions_form();
		
		
		$html = "
		
		<div class='row justify-content-md-center' post_id='$this->id' command='bio_edit_article'>
			<ul class='nav nav-pills mb-3 justify-content-center text-center' id='pills-tab' role='tablist'>
				<li class='nav-item'>
					<a class='nav-link active' id='pills-home-tab' data-toggle='pill' href='#pills-home' role='tab' aria-controls='pills-home' aria-selected='true'>".
					  __("Settings" ).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' id='pills-profile-tab' data-toggle='pill' href='#pills-profile' role='tab' aria-controls='pills-profile' aria-selected='false'>".
					  __("Pupils", BIO).
					"</a>
				</li>
			</ul>
			<div class='tab-content col-12' id='pills-tabContent'>
				<div class='tab-pane fade show active' id='pills-home' role='tabpanel' aria-labelledby='pills-home-tab'>
					
					<div class='card'>
						<div >
						  <div class='card-body'>
							<div class='row'>
								<form class='cab_edit' >
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Title", BIO). "</subtitle>
									<div class='col-12'>
										<input type='text' class='form-control' name='post_title' value='".$this->get("post_title")."' />
									</div>		
									<div class='spacer-10'></div>
									<div class='col-12'>
										$questions
									</div>	
									<subtitle class='col-12'>". __("Course", BIO). "</subtitle>
									<div class='col-12'>".
										Bio_Course::wp_dropdown([
											"class" 	=> "form-control",
											"name"		=> "course_id",
											"selected"	=> $course	
										]).
									"</div>	
									<div class='spacer-10'></div>	
																
									<div class='col-12'>
										<input type='submit' class='btn btn-primary value='". __("Update", BIO) . "' />		
									</div>		
								</form>
							</div>
						  </div>
						</div>
					</div>	
				</div>	
				
				<div class='tab-pane fade' id='pills-profile' role='tabpanel' aria-labelledby='pills-profile-tab'>					
					<div class='card'>
						<div id='collapseTwo' class='collapse show' aria-labelledby='headingTwo' data-parent='#accordion'>
							<div class='card-body'>".
								static::users_box_func( $this->id, false ) . 
							"</div>
						</div>
					</div>					
				</div>
				
			</div>	
		
		
		
		
		
		";
		return $html;
	}
	static function search_question($search="")
	{
		global $wpdb;
		$query = "SELECT id, questiontext FROM bio_question WHERE questiontext LIKE '%$search%';";
		return $wpdb->get_results($query);
	}
	static function get_all_questions ( $pars )
	{
		return Bio_Test_API::get_questions($pars);
	}
	static function get_single_question ($id)
	{
		$data = Bio_Test_API::get_question($id);
		//$data[0]->bio_biology_theme = Bio_Biology_Theme::get_bio_themes((int)$data->bio_biology_theme_id);
		
		return $data;
	}
	static function get_answers_single_question ($id)
	{
		global $wpdb;
		$query = "SELECT * FROM bio_answer WHERE question_id=$id;";
		return $wpdb->get_results($query);
	}
	
	function get_test_categories()
	{
		$tk		= get_the_terms($this->id, BIO_TEST_CATEGORY_TYPE);
		$r = [];
		foreach($tk as $t)
		{
			$r[] = Bio_Test_Category::get_category( $t );
		}
		return $r;
	}
	static function get_user_result($user_id)
	{
		$res 		= Bio_Test_API::get_results_of_user( $user_id );
		$result 	= [ ];
		foreach($res as $r)
		{
			$test 	= Bio_Test::get_instance($r['test_id']);
			$r['test_title'] = $test->get("post_title");
			$tk		= get_the_terms($r['test_id'], BIO_TEST_CATEGORY_TYPE);
			$r[BIO_TEST_CATEGORY_TYPE] = [];
			foreach($tk as $t)
			{
				$r[BIO_TEST_CATEGORY_TYPE][] = Bio_Test_Category::get_category( $t );
			}
			$result[] = $r;
		}
		return $result;
	}
	
	
	static function bio_gq_get_fields( $fields, $class_name, $post_type, $isForInput )
	{
		if( $post_type == static::get_type() && $isForInput )
		{
			$fields['questions']['type'] = Type::listOf( Type::string() );
			//wp_die( $fields['questions']['type'] );
		}
		return $fields;
	}
	static function peql_array_special($class, $obj, $key, $isForInput)
	{
		if( $obj['class']['type'] == "Bio_Test" && $key == "questions" )
		{
			$class = $isForInput ? Type::listOf( Type::string() ) : PEGraphql::object_type("Bio_TestQuestion");
		}
		return $class;
	}
	static function peql_array_spesial_resolve( 
		$array,
		$root, 
		$args, 
		$context, 
		$info, 
		$class_name, 
		$child_type, 
		$key, 
		$SMC_Object_type, 
		$obj  
	)
	{
		global $wpdb;		
		if( $obj['class']['type'] == "Bio_Test" && $key == "questions" )
		{ 
			//Bio_Test_API::test_refresh($root['id']);
			$questions = Bio_Test_API::test_get( $root['id'], false, true  );
			//wp_die( $questions['questions'] );
			$array = [];
			foreach($questions['questions'] as $q)
			{
				$answers = [];
				
				foreach($q['answers'] as $answer)
				{
					$answers[] = [
						"id"			=> $answer['id'],
						"post_content"	=> $answer['text'],
						"order"			=> $answer['position'],
					];
				}
				$bth = Bio_Biology_Theme::get_instance( (int)$q['bio_biology_theme_id'] );
				//$bth = Bio_Biology_Theme::get_instance( 72 );
				
				$tests = $wpdb->get_results("SELECT test_id FROM `bio_test_question` WHERE question_id='" . $q['id'] . "'");
				$bt = [];
				foreach( $tests as $test )
				{
					$bt[] = Bio_Test::get_single_matrix( $test->test_id );
				}  
				
				$question = [
					"id"			=> $q['id'],
					"post_title"	=> $q['name'],
					"post_content"	=> $q['questiontext'],
					"thumbnail"		=> $q['image_url'] ? get_bloginfo("url") . "/wp-content/uploads/" . $q['image_url'] : "",
					"image_id"		=> $q['image_id'],
					"type"			=> $q['type'],
					"penalty"		=> $q['penalty'],
					"post_author"	=> Bio_User::get_user( $q['author_id'] ),
					BIO_TEST_TYPE	=> $bt,
					BIO_BIOLOGY_THEME_TYPE 	=> $bth->get_single_matrix(),
					"bio_biology_theme_id" 	=> 72, //$q['bio_biology_theme_id'],
					"answers"		=> $answers 
				];
				$array[]	= $question;				
			}
		}
		
		return $array;
	}
	
	
	static function exec_pegql()
	{
		PEGraphql::add_object_type( 
			[
				"name"			=> "Bio_TestQuestion",
				"description"	=> __( "Test's Question", BIO ),
				"fields"		=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"name"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'name', BIO ), 
						'name' => 'post_title' 
					],
					"post_title"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'name', BIO ), 
						'name' => 'post_title' 
					],
					"questiontext"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'questiontext', BIO ), 
						'name' => 'post_content' 
					],
					"image_url"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'bio_biology_theme_id', BIO ),
						"name" => "thumbnail"
					],
					"author_id"	=> [ 
						'type' => PEGraphql::object_type("User"),	
						'description' 	=> __( 'Author of Question', BIO ), 
						'name' => 'post_author'  
					],
					"single_test_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'single_test_id', BIO )
					],
					"type"		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Multichoice, or more...', BIO )
					],
					"answernumbering"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'answernumbering', BIO )
					],
					"image_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'image_id', BIO )
					], 
					"correctfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'correctfeedback', BIO )
					],
					"defaultgrade"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'defaultgrade', BIO )
					],
					"generalfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'generalfeedback', BIO )
					],
					"hidden"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'hidden. Boolean', BIO )
					],
					"incorrectfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'incorrectfeedback function. Not use', BIO )
					],
					"partiallycorrectfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'partiallycorrectfeedback', BIO )
					],
					"penalty"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'float', BIO )
					],
					"shuffleanswers"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'Is shuffle answers?', BIO ), 
					],
					"single"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'single', BIO ), 
					],
					"is_deleted"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'is_deleted', BIO ), 
					],
					"position"	=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'position', BIO ), 
						'name' => 'order'  
					],
					"answers"	=> [
						'type' => Type::listOf( PEGraphql::object_type("Bio_TestQuestionAnswer") ), 	
						'description' 	=> __( 'Answers', BIO )
					],
					"bio_biology_theme_id"	=> [ 
						'type' => PEGraphql::object_type("Bio_Biology_Theme"), 	
						'description' 	=> __( 'bio_biology_theme_id', BIO )
					],
					BIO_BIOLOGY_THEME_TYPE	=> [ 
						'type' => PEGraphql::object_type("Bio_Biology_Theme"), 	
						'description' 	=> __( 'Theme', BIO )
					],
					BIO_TEST_TYPE	=> [ 
						'type' => Type::listOf( PEGraphql::object_type("Bio_Test")), 	
						'description' 	=> __( 'Tests', BIO )
					],
				]
			]
		);
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Bio_TestQuestionInput',
				'description'=> __( "Test's Question", BIO ),
				'fields' 	=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"single_test_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Parent Test', BIO )
					],
					"name"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'name', BIO ), 
						'name' => 'post_title' 
					],
					"questiontext"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'questiontext', BIO ), 
						'name' => 'post_content' 
					],
					"image_url"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'thumbnail', BIO ),
						"name" => "thumbnail"
					],
					"type"		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Multichoice, or more...', BIO )
					],
					"answernumbering"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'answernumbering', BIO )
					],
					"image_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'image_id', BIO )
					],
					"correctfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'correctfeedback', BIO )
					],
					"defaultgrade"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'defaultgrade', BIO )
					],
					"generalfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'generalfeedback', BIO )
					],
					"hidden"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'hidden. Boolean', BIO )
					],
					"incorrectfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'incorrectfeedback function. Not use', BIO )
					],
					"partiallycorrectfeedback"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'partiallycorrectfeedback', BIO )
					],
					"penalty"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'float', BIO )
					],
					"shuffleanswers"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'Is shuffle answers?', BIO ), 
					],
					"single"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'Question need single answer', BIO ), 
					],
					"is_deleted"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'is_deleted', BIO ), 
					],
					"position"	=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'position', BIO ), 
						'name' => 'order'  
					],
					"author_id"	=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'Author of Question', BIO ), 
						'name' => 'post_author'  
					],
					"answers"	=> [
						'type' => Type::listOf(Type::int()), 	
						'description' 	=> __( 'Answers', BIO )
					],
					"bio_biology_theme_id"	=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'bio_biology_theme_id', BIO ),
						"name" => BIO_BIOLOGY_THEME_TYPE
					],
				]
			]
		);
		
		PEGraphql::add_query( 
			'getBio_TestQuestion', 
			[
				'description' => __( 'Bio_TestQuestion', BIO ),
				'type' 		=> PEGraphql::object_type("Bio_TestQuestion"),
				'args'     	=> [ 
					"id"	=> [
						"type" => Type::string()
					]
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{
					return Bio_Question::get_single_matrix( $args['id'] );
				}
			] 
		);
		
		PEGraphql::add_mutation ( 
			'changeBio_TestQuestion', 
			[
				'description' 	=> __( "Change Bio_TestQuestion", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type("Bio_TestQuestionInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{	
					
					return true;
				}
			] 
		);
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Bio_TestQuestionAnswer",
				"description"	=> __( "Test Question Answer", BIO ),
				"fields"		=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"question_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Parent Question ', BIO )
					],
					"fraction"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'fraction', BIO )
					],
					"text"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'text', BIO ), 
						'name' => 'post_content' 
					],
					"is_deleted"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'is_deleted', BIO ), 
					],
					"is_subquestion"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'is subquestion in matching question', BIO ), 
					],
					"subquestion_answer_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "corresponding answer's ID", BIO ), 
					],
					"position"	=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'position', BIO ), 
						'name' => 'order'  
					]
				]
			]
		);
		
		//
		
		PEGraphql::add_mutation( 
			'set_test_results', 
			[
				'description' 	=> __( "Change User's password", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'id' 	=> [
						'description' => __( 'Teis ID', BIO ),
						'type' 	=> Type::string()
					],
					'start_time' 	=> [
						'description' => __( 'Started UNIX time in secs', BIO ),
						'type' 	=> Type::int()
					],
					'end_time' 	=> [
						'description' => __( 'Fnished UNIX time in secs', BIO ),
						'type' 	=> Type::int(),
					],
					'result' => [
						'type' => PEGraphql::input_type("Bio_TestResultInput"),
						'description' => __( "Results by questions", BIO ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					
					return true;
					
				}
			]
		);
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Bio_Answer",
				"description"	=> __( "Right answer of Question", BIO ),
				"fields"		=> [
					"question_id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Question ID', BIO ), 
						'name' => 'question_id' 
					],
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Answer ID', BIO ), 
						'name' => 'id' 
					],
					"text" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Answer test', BIO ), 
						'name' => 'post_content' 
					],
					"fraction" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Answer Fraction', BIO ), 
						'name' => 'fraction' 
					],
					"position" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'Answer positon in Question', BIO ), 
						'name' => 'position' 
					]
				]
			]
		);
		
		PEGraphql::add_mutation( 
			'getRightAnswer', 
			[
				'description' 	=> __( "return Right answer of Question", BIO ),
				'type' 			=> Type::listOf(PEGraphql::object_type("Bio_Answer")),
				'args'         	=> [
					'question_id' 	=> [
						'type' 	=>  Type::string(),
						'description' => __( 'Question ID', BIO )
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{ 
					$answers = Bio_Test_API::get_single_question_right($args['question_id']);
					foreach($answers as &$answer)
					{
						$answer['post_content'] = $answer['text'];
					}					
					// wp_die( $answers );
					return $answers;
				}
			]
		);
		
		
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Bio_TestResult",
				"description"	=> __( "A single test result", BIO ),
				"fields"		=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"questiontext"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'questiontext', BIO )
					],
					"type"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Question Type', BIO ) //"matching", "?"
					],
					"single"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'Question need single answer', BIO ), 
						'name' => 'post_content' 
					],
					"answers"	=> [ 
						'type' => Type::listOf( PEGraphql::object_type("Bio_TestResultAnswer") ), 	
						'description' 	=> __( 'array of answers', BIO ), 
					],
				]
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Bio_TestResultInput',
				'description'=> __( "A single test result", BIO ),
				'fields' 	=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"question_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Parent Question ', BIO )
					],
					"text"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'text', BIO ), 
						'name' => 'post_content' 
					],
					"is_deleted"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'is_deleted', BIO ), 
					],
					"is_subquestion"	=> [ 
						'type' => Type::boolean(), 	
						'description' 	=> __( 'is_subquestion', BIO ), 
					],
					"subquestion_answer_id"	=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "corresponding answer's ID", BIO ), 
					],
					"position"	=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'position', BIO ), 
						'name' => 'order'  
					]
				]
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Bio_TestResultAnswerInput',
				'description'=> __( "A single test result answer", BIO ),
				'fields' 	=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"fraction"	=> [						
						'type' => Type::string(), 	
						'description' 	=> __( 'fraction', BIO )
					],
					"subquestion_answer_id"	=> [						
						'type' => Type::string(), 	
						'description' 	=> __( 'subquestion_answer_id', BIO )
					]
				]
			]
		);
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Bio_UserTest",
				"description"	=> __( "Final results of all Users's Tests", BIO ),
				"fields"		=> [
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Test Result ID', BIO ), 
						'name' => 'id' 
					],
					"user_id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'User ID', BIO ), 
						'name' => 'id' 
					],
					"user_login" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'User login', BIO ), 
						'name' => 'id' 
					],
					"user_display_name" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'User display_name', BIO ), 
						'name' => 'id' 
					],
					"test" 		=> [ 
						'type' => PEGraphql::object_type("Bio_Test"), 	
						'description' 	=> __( 'Bio_Test', BIO ), 
					],
					"right_count" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'Number of successes', BIO ),
					],
					"start_time" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'Start time of last success', BIO ), 
					],
					"end_time" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'Finish time of last success', BIO ),
					],
					"credits" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( 'credits', BIO ), 
					],
					"bio_test_category"	=> [ 
						'type' => Type::listOf( PEGraphql::object_type("Bio_Test_Category") ), 	
						'description' 	=> __( 'test category', BIO ), 
					],
					"questions"	=> [ 
						'type' => Type::listOf( PEGraphql::object_type("Bio_UserTestQuestionResult") ), 	
						'description' 	=> __( 'results', BIO ), 
					]
				]
			]
		);
		PEGraphql::add_query( 
			'getBio_UserTest', 
			[
				'description' => __( "Final results of all Users's Tests", BIO ),
				'type' 		=> Type::listOf(PEGraphql::object_type("Bio_UserTest")),
				'args'     	=> [ 
					"id"	=> [
						"type" => Type::string()
					]
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{	
					$results = static::get_user_result($args['id']);
					//wp_die($results);
					$ret = [];
					foreach($results as $result)
					{
						$ret[] = [
							"id"				=> $result['id'],
							"user_id"			=> $result['user_id'],
							"user_login"		=> $result['user_login'],
							"user_display_name"	=> $result['user_display_name'],
							'test'				=> Bio_Test::get_single_matrix($result['test_id']),
							"right_count"		=> (int)$result['right_count'],
							"start_time"		=> (int)$result['start_time'],
							"end_time"			=> (int)$result['end_time'],
							"credits"			=> (int)$result['credits'],
							'bio_test_category'	=> $result['bio_test_category'],
							"questions"			=> []
						];
					}
					//wp_die($ret);
					return $ret;
				}
			] 
		);
		
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Bio_UserTestQuestionResult",
				"description"	=> __( "User's result of single Question Test", BIO ),
				"fields"		=> [
					"id"				=> 
					[
						"type"			=> Type::string(),
						'description'	=> __( 'Question ID', BIO )
					],
					"post_title"				=> 
					[
						"type"			=> Type::string(),
						'description'	=> __( 'Question ID', BIO )
					],
					"answers"			=> [
						"type"			=> Type::listOf(Type::string()),
						'description'	=> __( 'list of answers ID', BIO )
					],
					"right"			=> [
						"type"			=> Type::listOf(Type::string()),
						'description'	=> __( 'list of answers ID', BIO )
					]
				]
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"			=> "Bio_UserTestQuestionResultInput",
				"description"	=> __( "User's result of single Question Test", BIO ),
				"fields"		=> [
					"id"				=> 
					[
						"type"			=> Type::string(),
						'description'	=> __( 'Question ID', BIO )
					],
					"post_title"				=> 
					[
						"type"			=> Type::string(),
						'description'	=> __( 'Question ID', BIO )
					],
					"answers"			=> [
						"type"			=> Type::listOf(Type::string()),
						'description'	=> __( 'list of answers ID', BIO )
					]
				]
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"			=> "Bio_UserTestResultInput",
				"description"	=> __( "User's result of Test", BIO ),
				"fields"		=> [
					"test"			=> 
					[
						"type"			=> Type::string(),
						'description'	=> __( 'Test ID', BIO )
					],
					"questions"		=> [
						"type"			=> Type::listOf(PEGraphql::input_type("Bio_UserTestQuestionResultInput")),
						'description'	=> __( "List of questions' results", BIO )
					],
					"start_time"	=> [
						"type"			=> Type::int()
					],
					"end_time"	=> [
						"type"			=> Type::int()
					],
					"credits"	=> [
						"type"			=> Type::string()
					],
					"bio_test_category"	=> [
						"type"			=> Type::listOf(Type::string())
					]					
				]
			]
		);
		PEGraphql::add_mutation( 
			'compareTest', 
			[
				'description' 	=> __( "Final compare Test result", BIO ),
				'type' 			=> PEGraphql::object_type("Bio_UserTest"),
				'args'         	=> [
					'result' 	=> [
						'type' 	=>  PEGraphql::input_type("Bio_UserTestResultInput"),
						'description' => __( 'User unique identificator', BIO )
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{ 
					$credits = 0;
					$right_count = 0;
					$user 	= wp_get_current_user();
					$test	= Bio_Test::get_single_matrix( $args['result']['test'] );
					$testObj = Bio_Test::get_instance($args['result']['test']);
					
					//wp_die( $args['result']);
					
					$res 	= $args['result']['questions'];
					$questions = [];
					$trues	= Bio_Test_API::test_get( $args['result']['test'], false, true );
					//wp_die($trues['questions']);						
					foreach($res as &$re)
					{
						$question = [
							"question_id" => $re['id']
						];
						$right = [];
						$cred = 0;
						$answered = false;
						foreach($trues['questions'] as $t)
						{
							if($t['id'] == $re['id'])
							{
								
								$question["answers"] = [ ];
								$answer_answer = false;
								//wp_die( $t["answers"] );
								foreach( $t["answers"] as $answer )
								{
									if( $answer['fraction'] > 0 )
									{
										$right[] = $answer['id'];
										//wp_die([$answer['id'], $re['answers'] ]);
										if(in_array($answer['id'], $re['answers']))
										{
											$credits += $answer['fraction'];
											$right_count++;
											$answered = true;
											$answer_answer = true;
										}
									}
									$question["answers"][] = [
										"answer_id" => $answer['id'],
										"checked" => $answer_answer,
										"subquestion_answer_id" => $re['id']
									];
								}
							}
						}
						$re['right'] = $right; 
						//wp_die($re);
						
						$question['question_id'] = $re['id']; 
						$question['answered'] = $answered;							
						$questions[] = $question;
					}
					//wp_die( $questions );
					/*
					wp_die(
						Bio_Test_API::add_result(
							$args['result']['test'], 
							$user->ID, 
							$args['result']['start_time'],
							$args['result']['end_time'], 
							$res
						)
					);
					*/
					
					
					return [
						"id" 				=> -1,
						"user_id" 			=> $user->ID,
						"user_login" 		=> $user->user_login,
						"user_display_name"	=> $user->display_name,
						"test" 				=> $test,
						"right_count" 		=> $right_count,
						"start_time" 		=> $args['result']['start_time'],
						"end_time" 			=> $args['result']['end_time'],
						"credits" 			=> 200,
						"bio_test_category"	=> $testObj->get_test_categories() ,
						"questions"			=> $res
					];
				}
			]
		);
	}
	
}