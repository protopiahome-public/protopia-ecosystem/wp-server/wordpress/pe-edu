<?php
class Bio_Class extends SMC_Taxonomy
{
	static function get_type()
	{
		return BIO_CLASS_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 19);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 19);		
		add_action( BIO_CLASS_TYPE.'_edit_form_fields', 		array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_'.BIO_CLASS_TYPE, 					array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_'.BIO_CLASS_TYPE, 					array( __CLASS__, 'save_ctg'), 10);	
		add_filter("get_single_matrix",		[__CLASS__, "get_single_matrix_handler"], 10, 3);
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( "post", BIO_ARTICLE_TYPE, BIO_EVENT_TYPE, BIO_TEST_TYPE ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Class", BIO),
					'singular_name'     => __("Class", BIO),
					'search_items'      => __('search Class', BIO),
					'all_items'         => __('all Classes', BIO),
					'view_item '        => __('view Class', BIO),
					'parent_item'       => __('parent Class', BIO),
					'parent_item_colon' => __('parent Class:', BIO),
					'edit_item'         => __('edit Class', BIO),
					'update_item'       => __('update Class', BIO),
					'add_new_item'      => __('add Class', BIO),
					'new_item_name'     => __('new Class Name', BIO),
					'menu_name'         => __('Class', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Classes", BIO), 
			__("Classes", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-".BIO_CLASS_TYPE."", __("Classes", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
	
	static function add_ctg( $term, $tax_name )
	{
		if($term)
		{
			$term_id = $term->term_id;
			$order  = get_term_meta($term_id, "order", true);
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("Order", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" value="<?php echo $order; ?>" name="order" id="order"/>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "order", $_POST['order']);
	}
    static function delete( $post_id )
    {
        $post_id = (int)$post_id;
        wp_delete_term( $post_id, static::get_type() );
//        update_term_meta($post_id, "icon", $data["icon"]);
        return $post_id;
    }

    static function update( $data, $post_id )
    {
        $post_id = (int)$post_id;
        $data["name"] = $data["post_title"];// ? $data["post_title"] : $data["name"];
        wp_update_term( $post_id, static::get_type(), array(
            'name' 			=> $data["name"],
            'description' 	=> $data["description"],
        ));
        update_term_meta($post_id, "icon", $data["icon"]);
        update_term_meta($post_id, "order", (int)$data["name"]);
        return $post_id;
    }
    static function insert( $data )
    {
        $data['name'] = $data['post_title'];
        $post_id = wp_insert_term(
            $data["name"], static::get_type(),
            array(
				'description' => $data["description"]
        ) );
        update_term_meta($post_id['term_id'], "icon", $data["icon"]);
        update_term_meta($post_id['term_id'], "order", (int)$data["name"]);
        return $post_id;
    }



    static function get_classes($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, BIO_CLASS_TYPE);
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;

        $c['id']			= $course->term_id;
        $c['ID']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['post_content']	= $course->description;

        return $c;
    }

    static function get_class($class)
    {
        if(is_numeric($class))
        {
            $class = get_term($class, BIO_CLASS_TYPE);

        }
        $c = [];
        $c['id']			= $class->term_id;
        $c['ID']			= $class->term_id;
        $c['post_title']	= $class->name;
        return $c;

    }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
        $courses	= [];

        switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
                    Bio_Class::update($pars, $code);
                    $courses[]	= static::get_classes( $code );
                    $update = 'success';
                }
				else
				{
                    $update = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) 				
				{
                    Bio_Class::delete($code);
                    $msg = __("Class removed succesfully", BIO);
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "create":
                if(is_numeric($code)) 
				{
                    Bio_Class::update($pars, $code);
                    $cat	= static::get_classes( $code );
					$msg = sprintf( __("Class «%s» updated succesfully", BIO), $cat['post_title'] ); 
					$courses[]	= $cat;					
                }
				else
				{
                    $class = Bio_Class::insert($pars);
                    $courses[]			= static::get_class($class);
                    $msg = __("Class inserted succesfully", BIO);
                }
                break;
            case "read":
            default:
//                        $code = (int)$code;
                if(is_numeric($code)){
                    $c							= static::get_classes( $code );

                    $courses[]					= $c;
                    /**/
                    $articles = [];
                    $all 	= Bio_Article::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "id", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        isset($pars['author'])			? $pars['author']		: -1,		// "",
                        [ "bio_class" => $code ]

                    );
                    foreach($all as $p)
                    {
                        $a 					= Bio_Article::get_article( $p, false );
                        $courses[]			= $a;
                    }
                }
				else
				{
                    $terms = get_terms( array(
                        'taxonomy'      => BIO_CLASS_TYPE,
                        'orderby'       => "meta_value_num",
						"meta_key"		=> "order",
                        'order'         => 'ASC',
                        'hide_empty'    => false,
                        'number'        => '',
                        'fields'        => 'all',
                        'count'         => false,
                        'hierarchical'  => true,
                        'child_of'      => 0,
                    ) );
                    foreach($terms as $c)
                    {
                        $courses[]	= static::get_classes( $c );
                    }
                    break;
                }
                break;
        }
        return Array (
            "classes" => $courses,
            "articles" => $articles,
            "msg" => $msg,
            "id" => $code,
            "update"=> $update
        );
    }
	static function get_single_matrix_handler($matrix, $term, $type)
	{
		$ins = static::get_instance($term);
		$matrix['order']	= (int)$ins->get_meta("order");
		return $matrix;
	}
}
	