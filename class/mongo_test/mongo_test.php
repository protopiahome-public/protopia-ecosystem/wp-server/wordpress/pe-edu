<?php
ini_set("display_errors", 1);
 // open connection to MongoDB server
require 'vendor/autoload.php'; // ���������� �������������� ������� Composer

class MongoMeta
{
	var $client;
	var $db;
	
	function __construct($db, $client = "mongodb://localhost:27017")
	{
		$this->client = new MongoDB\Client($client);
		$this->db = $this->client->selectDatabase($db);
	}
	
	function get_meta($type, $id)
	{
		$collection = $this->db->selectCollection($type);
		$results = $collection->find(["wp_id" => $id]);
		$results = $this->arrayCastRecursive($results->toArray());
		foreach ($results as &$result)
		{
			unset($result["_id"]);
		}
		return $results;
	}
	
	function set_meta($type, $id, $input)
	{
		$collection = $this->db->selectCollection($type);
		$results = $collection->findOneAndUpdate(["wp_id" => $id], ["\$set" => $input]);
		if (!count($results)) {
			$input["wp_id"] = $id;
			$results = $collection->insertOne($input);
		}
		return $results;
	}

	function arrayCastRecursive($array)
	{
		$array = (array)$array;
		foreach ($array as $key => $value) {
			if (is_array($value) || is_object($value)) {
				$array[$key] = $this->arrayCastRecursive($value);
			}
		}
		return $array;
	}
}

$mongometa = new MongoMeta("wpfest");
$mongometa->set_meta("project", "5c7e4c676266ed7f8bae9fe3", ["a" => "a", "c" => null]);
print_r($mongometa->get_meta("project", "5c7e4c676266ed7f8bae9fe3"));