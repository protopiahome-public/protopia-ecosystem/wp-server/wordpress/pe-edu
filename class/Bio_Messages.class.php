<?php
define("BIO_ACCESS_ERROR_MESSAGE", "You not access rights." );
define("BIO_ERROR_TEST_TITLE", "Message about error in Test %s" );
define("BIO_ERROR_TEST_MESSAGE", "User find error in test");
define("BIO_USER_CHANGE_ROLES_TILE", "About change your capabilities");
define("BIO_USER_CHANGE_ROLES", "Administrator change your capabilities. Now You have roles: %s.");
define("BIO_RESTORE_PASSWORD_TITLE", "Restore your password");
define("BIO_SELF", "users");

class Bio_Messages
{
	static $alredy_request;
	static $send_request;
	static function send_request($course_id)
	{
		return "<div class='btn btn-primary btn-lg add_course' cid='$course_id'>" . 
			__("Request permission to join the course", BIO) .
		"</div>";
	}
	static function successful_conglom_access()
	{
		return __("You add new pupil to Event. We sent email to him.", BIO) ;
	}
	static function change_password()
	{
		return __("Put your new password", BIO) ;
	}
	static function last_name()
	{
		return __("Put your last name", BIO) ;
	}
	static function first_name()
	{
		return __("Put your first name", BIO) ;
	}
	static function old_password()
	{
		return __("Put your old password", BIO) ;
	}
	static function get_you_event_member()
	{
		return __("You are allready member of Event. Welcome!", BIO) ;
	}
	static function get_you_event_requested()
	{
		return __("You are allready sent request to Event. We wait answer from owners.", BIO) ;
	}
	static function register_anonim()
	{
		return __("Register successfull", BIO) ;
	}
	static function send_email_exists($email)
	{
		return "<div>" . 
			sprintf(__("User with email %s allredy exsts. Put anover email.", BIO), $email) .
		"</div>";
	}
	static function send_email_no_exists($email)
	{
		return sprintf(__("User with email %s not exsts. You may register new account used it.", BIO), $email) ;
	}
	static function send_no_email($email)
	{
		return "<div >" . 
			sprintf(__("%s - no email", BIO), $email) .
		"</div>";
	}
	static function alredy_request($course_id)
	{
		return "
		<div class='alert alert-success' role='alert'>".
			__("You have already sent a request to join the Course. Do you want to withdraw the request?", BIO) . 
			"<p>
				<div class='btn btn-danger leave_req' cid='$course_id'>" . 
					__("withdraw request", BIO) . 
				"</div>
		</div>";
	}
	static function init()
	{
			
	}
}