<?php
function save_event( $name = "event", $data_array = [] )
{
	// Подключаем класс для работы с excel
	require_once ( BIO_REAL_PATH . "/lib/PHPExcel.php");
	// Подключаем класс для вывода данных в формате excel
	require_once ( BIO_REAL_PATH . "/lib/PHPExcel/Writer/Excel5.php");

	 
	// Создаем объект класса PHPExcel
	$xls = new PHPExcel();
	// Устанавливаем индекс активного листа
	$xls->setActiveSheetIndex(0);
	// Получаем активный лист
	$sheet = $xls->getActiveSheet();
	// Подписываем лист
	$sheet->setTitle( "name" );
	//$PHPExcel_Style->getFont()->setSize(20);
	// Вставляем текст в ячейку A1
	$sheet->setCellValue( "A1", $name );
	$sheet->getStyle('A1')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');
	
	// Объединяем ячейки
	$sheet->mergeCells('A1:E1');
	 
	// Выравнивание текста
	$sheet->getStyle('A1')->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
	
	//$PHPExcel_Style->getFont()->setSize(12);
	$i = 1;
	foreach($data_array as $user)
	{
		$sheet->setCellValueByColumnAndRow( 0, $i+1, $user['user_email'] );								  
		$sheet->setCellValueByColumnAndRow( 1, $i+1, $user['display_name'] );								  
		$sheet->setCellValueByColumnAndRow( 2, $i+1, $user['credits'] );								  
		$sheet->setCellValueByColumnAndRow( 3, $i+1, $user['duration'] );								  
		$sheet->setCellValueByColumnAndRow( 4, $i+1, $user['right_count'] );
		$sheet->getColumnDimension("A")->setAutoSize(true);
		$sheet->getColumnDimension("B")->setAutoSize(true);
		$sheet->getColumnDimension("C")->setWidth(10);
		$sheet->getColumnDimension("D")->setWidth(10);
		$sheet->getColumnDimension("E")->setWidth(10);
		$i++;
	}
	$x = new PHPExcel_Writer_Excel5($xls);
	$x->save( "$name.xls" );
	
	$zip = new ZipArchive;
	if ($zip->open(ABSPATH . "temp/$name.zip", ZipArchive::CREATE) === TRUE) 
	{
		$zip->addFile( "$name.xls" );
		$zip->close();
	}
	@unlink( "$name.xls" );
	return get_bloginfo("url") . "/temp/$name.zip";
}