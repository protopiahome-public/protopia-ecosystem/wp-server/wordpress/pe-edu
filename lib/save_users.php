<?php
function save_users( $name = "users" )
{
	global $wpdb, $booooo;
	$query = "
	SELECT u.user_email, u.ID, u.display_name, pu1.meta_value AS birthday, pu2.meta_value AS phonenumber, pu3.meta_value AS caps, pu4.meta_value AS class, pu5.meta_value AS town, pu6.meta_value AS gender
	FROM {$wpdb->users} AS u
	LEFT JOIN " . $wpdb->prefix . "usermeta AS pu1 ON pu1.user_id=u.ID AND  pu1.meta_key='birthday'
	LEFT JOIN " . $wpdb->prefix . "usermeta AS pu2 ON pu2.user_id=u.ID AND  pu2.meta_key='phonenumber'
	LEFT JOIN " . $wpdb->prefix . "usermeta AS pu3 ON pu3.user_id=u.ID AND  pu3.meta_key='por_capabilities'
	LEFT JOIN " . $wpdb->prefix . "usermeta AS pu4 ON pu4.user_id=u.ID AND  pu4.meta_key='studentclass'
	LEFT JOIN " . $wpdb->prefix . "usermeta AS pu5 ON pu5.user_id=u.ID AND  pu5.meta_key='town'
	LEFT JOIN " . $wpdb->prefix . "usermeta AS pu6 ON pu6.user_id=u.ID AND  pu6.meta_key='gender'";
	$data_array = $wpdb->get_results( $query, ARRAY_A );
	
	// Подключаем класс для работы с excel
	require_once ( BIO_REAL_PATH . "/lib/PHPExcel.php");
	// Подключаем класс для вывода данных в формате excel
	require_once ( BIO_REAL_PATH . "/lib/PHPExcel/Writer/Excel5.php");

	//$booooo = $query;
	// Создаем объект класса PHPExcel
	$xls = new PHPExcel();
	// Устанавливаем индекс активного листа
	$xls->setActiveSheetIndex(0);
	// Получаем активный лист
	$sheet = $xls->getActiveSheet();
	// Подписываем лист
	$sheet->setTitle( "name" );
	//$PHPExcel_Style->getFont()->setSize(20);
	// Вставляем текст в ячейку A1;
	$sheet->setCellValue( "A1", $name );
	$sheet->getStyle('A1')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');
	
	// Объединяем ячейки
	$sheet->mergeCells('A1:H1');
	 
	// Выравнивание текста
	$sheet->getStyle('A1')->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
	
	$sheet->setCellValue( "A2", __("Email", BIO) );
	$sheet->getStyle('A2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('A2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "B2", __("Display name", BIO) );
	$sheet->getStyle('B2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('B2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "C2", __("Birthday date", BIO) );
	$sheet->getStyle('C2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('C2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "D2", __("Gender", BIO) );
	$sheet->getStyle('D2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('D2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "E2", __("City", BIO) );
	$sheet->getStyle('E2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('E2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "F2", __("Phone", BIO) );
	$sheet->getStyle('F2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('F2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "G2", __("Class", BIO) );
	$sheet->getStyle('G2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('G2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	$sheet->setCellValue( "H2", __("Roles", BIO) );
	$sheet->getStyle('H2')->getFill()->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
	$sheet->getStyle('H2')->getFill()->getStartColor()->setRGB('AAAAAA');
	
	
	$i = 2;
	$booooo = [];
	foreach($data_array as $user)
	{
		$sheet->setCellValueByColumnAndRow( 0, $i+1, $user['user_email'] );								  
		$sheet->setCellValueByColumnAndRow( 1, $i+1, $user['display_name'] );								  
		$sheet->setCellValueByColumnAndRow( 2, $i+1, $user['birthday'] );								  
		$sheet->setCellValueByColumnAndRow( 3, $i+1, __($user['gender'] == "male"  ? "Male" : "Female", BIO) );
		$sheet->setCellValueByColumnAndRow( 4, $i+1, $user['town'] );								  
		$sheet->setCellValueByColumnAndRow( 5, $i+1, $user['phonenumber'] );	
		$class = get_term_by("id", $user['class'], BIO_CLASS_TYPE);
		$sheet->setCellValueByColumnAndRow( 6, $i+1, !is_wp_error($class) ? $class->name : "");	
		$cap = [];
		$boo = unserialize( $user['caps'] );
		foreach( $boo as $key=>$val)
		{
			if($key != "subscriber")
				$cap[] = __($key, BIO);
		}	
		$sheet->setCellValueByColumnAndRow( 7, $i+1, implode( ", ", $cap ) );	
		$sheet->getColumnDimension("A")->setAutoSize(true);
		$sheet->getColumnDimension("B")->setAutoSize(true);
		$sheet->getColumnDimension("C")->setAutoSize(true);
		$sheet->getColumnDimension("D")->setAutoSize(true);
		$sheet->getColumnDimension("E")->setAutoSize(true);
		$sheet->getColumnDimension("F")->setAutoSize(true);
		//$sheet->getColumnDimension("G")->setAutoSize(true);
		$sheet->getColumnDimension("H")->setAutoSize(true);
		$i++;
	}
	$x = new PHPExcel_Writer_Excel5($xls);
	$x->save( "$name.xls" );
	
	$zip = new ZipArchive;
	if ($zip->open(ABSPATH . "temp/$name.zip", ZipArchive::CREATE) === TRUE) 
	{
		$zip->addFile( "$name.xls" );
		$zip->close();
	}
	@unlink( "$name.xls" );
	return get_bloginfo("url") . "/temp/$name.zip";
}