<?php 

class Bio_Message extends SMC_Post
{
	static function get_type()
	{
		return BIO_MESSAGE_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 98.5);	
		add_filter('before_change_gq_post', 	[ __CLASS__, 'before_change_gq_post' ], 10,8);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Message", BIO), // Основное название типа записи
			'singular_name'      => __("Message", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Message", BIO), 
			'all_items' 		 => __('Messages', BIO),
			'add_new_item'       => __("add Message", BIO), 
			'edit_item'          => __("edit Message", BIO), 
			'new_item'           => __("add Message", BIO), 
			'view_item'          => __("see Message", BIO), 
			'search_items'       => __("search Message", BIO), 
			'not_found'          => __("no Messages", BIO), 
			'not_found_in_trash' => __("no Messages in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Messages", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ BIO_COURSE_TYPE ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 92.5,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title','editor','author','thumbnail','excerpt','comments' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	static function before_change_gq_post( $return, $root, $args, $context, $info, $class_name, $post_type, $val )
	{
		if( !isset($args['id']) && $post_type== static::get_type() )
		{
			$post_args = $args['input'];
			
			$post_args["post_author"] = -1;
			$post_args["post_type"] = strtolower($class_name);
			$post_args['post_status']   = $args['input']['post_status']
					? 
					$args['input']['post_status']
					: 
					"publish";
			$id = static::insert($post_args);
			if(!is_wp_error($id))
			{
				return Bio_Message::get_single_matrix($id);
			}
			else
			{
				wp_die($id);
			}
		}
		return null;
		
	}
}