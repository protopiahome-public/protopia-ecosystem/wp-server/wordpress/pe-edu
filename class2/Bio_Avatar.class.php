<?php 

class Bio_Avatar extends SMC_Post
{
	static function get_type()
	{
		return BIO_AVATAR_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 98.5);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Avatar", BIO), // Основное название типа записи
			'singular_name'      => __("Avatar", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Avatar", BIO), 
			'all_items' 		 => __('Avatars', BIO),
			'add_new_item'       => __("add Avatar", BIO), 
			'edit_item'          => __("edit Avatar", BIO), 
			'new_item'           => __("add Avatar", BIO), 
			'view_item'          => __("see Avatar", BIO), 
			'search_items'       => __("search Avatar", BIO), 
			'not_found'          => __("no Avatars", BIO), 
			'not_found_in_trash' => __("no Avatars in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Avatars", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 91,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title', 'thumbnail' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}