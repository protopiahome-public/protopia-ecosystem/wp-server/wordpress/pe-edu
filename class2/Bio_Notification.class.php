<?php 

class Bio_Notification extends SMC_Post
{
	static function get_type()
	{
		return BIO_NOTIFICATION_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 98.6);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Notification", BIO), // Основное название типа записи
			'singular_name'      => __("Notification", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Notification", BIO), 
			'all_items' 		 => __('Notifications', BIO),
			'add_new_item'       => __("add Notification", BIO), 
			'edit_item'          => __("edit Notification", BIO), 
			'new_item'           => __("add Notification", BIO), 
			'view_item'          => __("see Notification", BIO), 
			'search_items'       => __("search Notification", BIO), 
			'not_found'          => __("no Notifications", BIO), 
			'not_found_in_trash' => __("no Notifications in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Notifications", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 92.6,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}