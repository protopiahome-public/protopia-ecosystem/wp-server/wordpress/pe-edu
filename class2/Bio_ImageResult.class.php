<?php 

class Bio_ImageResult extends SMC_Post
{
	static function get_type()
	{
		return BIO_IMAGE_RESULT_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 98.3);
		add_filter("smc_post_get_all_args", 		[ __CLASS__, "smc_post_get_all_args"], 11 );	
		parent::init();
	}
	static function smc_post_get_all_args( $args )
	{
		if(current_user_can("manage_options"))	return $args;
		if( $args["post_type"]  == static::get_type())
		{
			if( !isset( $args["tax_query"] ))
			{
				$args["tax_query"] = [ "relation" => "OR" ];
			}
			$args["tax_query"][] = [
				"taxonomy"		=> BIO_FACULTET_TYPE,
				"fields"		=> "id",
				"terms"			=> Bio_Facultet::get_user_facultet( null, true )
			];
			//wp_die( $args );
		}
		return $args;
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Image Result", BIO), // Основное название типа записи
			'singular_name'      => __("Image Result", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Image Result", BIO), 
			'all_items' 		 => __('Image Results', BIO),
			'add_new_item'       => __("add Image Result", BIO), 
			'edit_item'          => __("edit Image Result", BIO), 
			'new_item'           => __("add Image Result", BIO), 
			'view_item'          => __("see Image Result", BIO), 
			'search_items'       => __("search Image Result", BIO), 
			'not_found'          => __("no Image Results", BIO), 
			'not_found_in_trash' => __("no Image Results in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Image Results", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				"taxonomies"		 => [BIO_FACULTET_TYPE, BIO_COURSE_TYPE],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 92.4,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title', "thumbnail", "editor" ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}