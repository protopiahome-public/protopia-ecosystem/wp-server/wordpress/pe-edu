<?php
function get_bio_cab_plate()
{
	$html = "
		<div class='row'>
			<div class='col-12'>";
	
	if(Bio_user::is_user_role("contributor"))
	{
		$courses	= Bio_User::get_courses( get_current_user_id() );
		if( count( $courses ) )
			foreach($courses as $course)
			{
				$crs .= "
				<div class='row'>
					<div class='col-4'><a href='" . get_term_link( (int)$course['term_id'], BIO_COURSE_TYPE ) . "'>" . $course['name'] . "</a></div>
					<div class='col-8'>" . 
						$course['description'] .
					"</div>
				</div>";
			}
		else
		{
			$all = Bio_Course::get_all();
			$courses = "<ul class='bio_ans-form'>";
			foreach($all as $c)
			{
				$courses .= "
				<li>
					<a href='" . get_term_link( (int)$c->term_id, BIO_COURSE_TYPE ) . "'>" . $c->name . "</a>
				</li>";
			}
			$courses .= "</ul>";
			
			$crs = "
			<div class='alert alert-success text-center w-100' role='alert'>".
				"<div class='bio_descr w-100'>".
					__("In this section you can manipulate with Courses that you subscribed.", BIO).
				"</div>".
				__("No your Courses subscribes", BIO) . 
				"<p>" . 
				__("Attend courses and subscribe:", BIO) .
				$courses .
			"</div>";
		}
		foreach($courses as $c)
		{
			$crs1 	.=  "
			<div class='row'>
				<div class='lead col-6'>" . 	$c->name . "</div>
				<div class='col-6'>" . 		$c->icon . "</div>
			</div>";
		}
	}
	$html .= "
			</div>
			<div class='col-12' id='cab_tab_cont'>
				$crs 
			</div>
		</div>";
	return $html;
}